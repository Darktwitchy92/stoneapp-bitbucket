package com.journaldev.barcodevisionapi;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class SplashScreen extends AppCompatActivity {

    ImageView iconn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        // Hide the Status Bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);





        Animation anime = AnimationUtils.loadAnimation(this,R.anim.bounce);


        iconn = findViewById(R.id.splashicon);
        iconn.startAnimation(anime);
        CountDownTimer cntr_aCounter = new CountDownTimer(3000, 1000) {

            MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.into_water);

            public void onTick(long millisUntilFinished) {

                mp.start();
            }

            public void onFinish() {
                //code fire after finish
                mp.stop();
            }
        };cntr_aCounter.start();



        Thread thread = new Thread(){

            @Override
            public void run() {
                try {
                    sleep(5000);
                    startActivity(new Intent(SplashScreen.this, Textwelcome.class));
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        thread.start();
    }
}
