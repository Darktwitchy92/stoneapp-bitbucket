package com.journaldev.barcodevisionapi;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;
import java.util.ArrayList;

import static com.google.android.gms.vision.CameraSource.CAMERA_FACING_BACK;

public class ScannedBarcodeActivity extends AppCompatActivity{


    SurfaceView surfaceView;
    SurfaceView transview;
    TextureView t;
    public TextView txtBarcodeValue;
    private BarcodeDetector barcodeDetector;
    private CameraSource cameraSource;
    private CameraSource cameraSource2;
    private SurfaceHolder holderTransparent;
    public  Rect rec;
    public  int trigger = 0;
    public Frame outputFrame;
    private static int wid = 0, hgt = 0;
    public Bitmap newImage;
    public static String SERIALPREFS = "SERIALS";
    public static String c = "yes";
    public CountDownTimer countdown;

    TextView txtbtn;
    TextView manualser;


    public  Canvas canvas;




    private static final int REQUEST_CAMERA_PERMISSION = 201;

    Button btnAction, btnserialhis, scanbtn;
    String intentData = "";
    String passover = "";

    boolean isEmail = false;


    private float RectLeft, RectTop,RectRight,RectBottom ;

    int  deviceHeight,deviceWidth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(AppCompatDelegate.getDefaultNightMode()==AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.lightertheme);
        }else setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_barcode);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN );


        initViews();


        txtbtn = findViewById(R.id.textbutton3);
        manualser = findViewById(R.id.textEDIT2);
        scanbtn = findViewById(R.id.scanbtn);

        startmessage();








        txtbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtbtn.setVisibility(View.GONE);
                manualser.setVisibility(View.VISIBLE);

            }
        });




//
                        transview.setZOrderMediaOverlay(true);
//
//                        //getting the device heigth and width
                      deviceWidth=getScreenWidth();

                       deviceHeight=getScreenHeight();
                        surfaceView.setSecure(true);


                        scanbtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                trigger = 1;
                            }
                        });














    }








    public void startmessage(){

        countdown = new CountDownTimer(10000, 10000) {

            public void onTick(long millisUntilFinished) {
              //  mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
               // mTextField.setText("done!");

                new AlertDialog.Builder(ScannedBarcodeActivity.this, R.style.MyAlertDialogStyle)
                        .setTitle("Try manually?")
                        .setMessage("Having Problems scanning? why not try manually.")


                        // Specifying a listener allows you to take an action before dismissing the dialog.
                        // The dialog is automatically dismissed when a dialog button is clicked.
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Continue with delete operation

                                Intent i = new Intent(ScannedBarcodeActivity.this, MainActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString("manu", c);
                                i.putExtras(bundle);
                                startActivity(i);

                            }
                        })

                        // A null listener allows the button to dismiss the dialog and take no further action.
                        .setNegativeButton(android.R.string.no, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        }.start();




    }

    public static int getScreenWidth() {

        return Resources.getSystem().getDisplayMetrics().widthPixels;

    }


    public static int getScreenHeight() {

        return Resources.getSystem().getDisplayMetrics().heightPixels;

    }


    private void initViews() {


        txtBarcodeValue = findViewById(R.id.txtBarcodeValue);
        surfaceView = findViewById(R.id.surfaceView);
        btnAction = findViewById(R.id.btnAction);
        btnserialhis = findViewById(R.id.btnHis);
        transview = findViewById(R.id.trans);






        btnserialhis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                    Intent i = new Intent(ScannedBarcodeActivity.this, SerialHistory.class);
                    startActivity(i);
                }


        });



        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sendser();

            }
        });

    }



    private void Draw()

    {

        holderTransparent = transview.getHolder();
        holderTransparent.setFormat(PixelFormat.TRANSLUCENT);

         canvas = holderTransparent.lockCanvas(null);

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

        paint.setStyle(Paint.Style.STROKE);

        paint.setColor(Color.GREEN);

        paint.setStrokeWidth(20);

        RectLeft = 60;

        RectTop = 100 ;

        RectRight = RectLeft+ deviceWidth-80;

        RectBottom =RectTop+ 300;

       rec=new Rect((int) RectLeft-60,(int)RectTop,(int)RectRight,(int)RectBottom);

        canvas.drawRect(rec,paint);

        wid = rec.width();
        hgt = rec.height();

         newImage = Bitmap.createBitmap(wid, hgt, Bitmap.Config.ARGB_8888);


        holderTransparent.unlockCanvasAndPost(canvas);



    }





    private void initialiseDetectorsAndSources() {






        Toast.makeText(getApplicationContext(), "Barcode scanner started", Toast.LENGTH_SHORT).show();












        barcodeDetector  = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.ISBN)
                .build();

        final MyBarcodeDetector mybarcodeDetector = new MyBarcodeDetector(barcodeDetector);
        BoxDetector boxdect = new BoxDetector(mybarcodeDetector,720, 500);


//        outputFrame = new Frame.Builder()
//                .setImageData(holderTransparent, transview.getWidth(),
//                        transview.getHeight(), ImageFormat.NV21)
//                .setId(mPendingFrameId)
//                .setTimestampMillis(mPendingTimeMillis)
//                .setRotation(mRotation)
//                .build();





        if (!boxdect.isOperational()) {
           // txtResultBody.setText("Detector initialisation failed");
            return;
        }



        cameraSource = new CameraSource.Builder(this, boxdect)
                .setRequestedPreviewSize(1280, 720)
                .setAutoFocusEnabled(true) //you should add this feature
               .setFacing(CAMERA_FACING_BACK )

                .build();








        surfaceView.getHolder().addCallback(new SurfaceHolder
                .Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {







                holder.setFixedSize(100, 200);


                //holder.lockCanvas( new Rect(70, 20, 110, 280) );



                try {
                    if (ActivityCompat.checkSelfPermission(ScannedBarcodeActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                        cameraSource.start(surfaceView.getHolder());

                    } else {
                        ActivityCompat.requestPermissions(ScannedBarcodeActivity.this, new
                                String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }


            }



            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {


                holder.setFixedSize(100, 200);



                // holder.lockCanvas( new Rect(70, 20, 110, 280) );



                try {
                    if (ActivityCompat.checkSelfPermission(ScannedBarcodeActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                        cameraSource.start(surfaceView.getHolder());
                    } else {
                        ActivityCompat.requestPermissions(ScannedBarcodeActivity.this, new
                                String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }



            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });

        transview.getHolder().addCallback(new SurfaceHolder
                .Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder3) {



                try {
                    if (ActivityCompat.checkSelfPermission(ScannedBarcodeActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {





                        Draw();

                        SurfaceHolder sholder = transview.getHolder();
                        Canvas canvas2 = sholder.lockCanvas();

                        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

                        paint.setStyle(Paint.Style.STROKE);

                        paint.setColor(Color.GREEN);

                        paint.setStrokeWidth(20);

                        RectLeft = 60;

                        RectTop = 100 ;

                        RectRight = RectLeft+ deviceWidth-80;

                        RectBottom =RectTop+ 300;

                        rec=new Rect((int) RectLeft-60,(int)RectTop,(int)RectRight,(int)RectBottom);

                        canvas.drawRect(rec,paint);

                        sholder.unlockCanvasAndPost(canvas2);







                        //  cameraSource.start(transview.getHolder());









                    } else {
                        ActivityCompat.requestPermissions(ScannedBarcodeActivity.this, new
                                String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }





            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });




        boxdect.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
                ///Toast.makeText(getApplicationContext(), "To prevent memory leaks barcode scanner has been stopped", Toast.LENGTH_SHORT).show();
//                barcodeDetector.detect(frame);
            }



            @Override
            public void receiveDetections(Detector.Detections<Barcode> Detections) {

                final SparseArray<Barcode> barcodes = Detections.getDetectedItems();

                if (barcodes.size() != 0) {

                    if(trigger==1){



                    // Get instance of Vibrator from current Context
                    Vibrator mVibrator  = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.scanner_beeps);

                    mp.start();
                    // This code snippet will cause the phone to vibrate "SOS" in Morse Code
                    // In Morse Code, "s" = "dot-dot-dot", "o" = "dash-dash-dash"
                    // There are pauses to separate dots/dashes, letters, and words
                    // The following numbers represent millisecond lengths
                    int dot = 200;      // Length of a Morse Code "dot" in milliseconds
                    int dash = 500;     // Length of a Morse Code "dash" in milliseconds
                    int short_gap = 200;    // Length of Gap Between dots/dashes
                    int medium_gap = 500;   // Length of Gap Between Letters
                    int long_gap = 1000;    // Length of Gap Between Words
                    long[] pattern = {
                            0,  // Start immediately
                            dot, dot
                    };


                    mVibrator.vibrate(pattern, -1);








                    txtBarcodeValue.post(new Runnable() {

                        @Override
                        public void run() {

                            if (barcodes.valueAt(0).email != null) {
                                txtBarcodeValue.removeCallbacks(null);
                                intentData = barcodes.valueAt(0).email.address;
                                txtBarcodeValue.setText(intentData);
                                isEmail = true;
                                btnAction.setText("ADD CONTENT TO THE MAIL");
                            } else {
                                isEmail = false;
                                btnAction.setText("Serial Found");
                                intentData = barcodes.valueAt(0).displayValue;
                                txtBarcodeValue.setText(intentData);

                                final globalvar globvar = (globalvar) getApplicationContext();

                                globvar.addserial(intentData);

                                ArrayList test = globvar.getserials();




                                sendser();

                            }
                        }
                    });


                    }
                }
            }
        });
    }

    public void sendser(){
        if(intentData != null) {

            Intent Intentbundle = new Intent(ScannedBarcodeActivity.this, searchParts.class);
            Bundle bundle = new Bundle();
            bundle.putString("data", intentData);
            Intentbundle.putExtras(bundle);
            startActivity(Intentbundle);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        cameraSource.release();
    }

    @Override
    protected void onResume() {
        super.onResume();
        trigger = 0;
        initialiseDetectorsAndSources();


    }

    @Override
    public void onBackPressed(){

       // Toast.makeText(this, "hello where back", Toast.LENGTH_SHORT).show();
        countdown.cancel();
        Intent i = new Intent(this,
                MainActivity.class);
                 startActivity(i);
                 trigger = 0;


                   }






}
