package com.journaldev.barcodevisionapi;

import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MaterialSearch extends AppCompatActivity {
    RequestQueue queue;


    private SimpleAdapter sa;
    ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            setTheme(R.style.lightertheme);
        } else setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.parts_inmachine);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Bundle newstuff = getIntent().getExtras();

        String partcode = newstuff.getString("Partcode");
        String account = newstuff.getString("account");
        String serial = newstuff.getString("serial");

        queue = Volley.newRequestQueue(this);
        final String URL = "https://support.stonegroup.co.uk/BarcodeAppPHP/materialSearchquery.php?serial=" + serial + "&part=" + partcode + "&acc=" + account;

        Log.v("serial :", newstuff.getString("serial"));

        StringRequest strreq = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // get response
                        Log.v("got", response);
                        //  Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();


                        try {
                            // Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();

                            JSONArray jsonarray = new JSONArray(response);

                            HashMap<String, String> item;
                            for (int i = 0; i < jsonarray.length(); i++) {

                                JSONObject jsonobject = jsonarray.getJSONObject(i);
                                String partcode = jsonobject.getString("ProductCode");
                                String PartMaterial = jsonobject.getString("PartMaterialCode");
                                String Matdesc = jsonobject.getString("MaterialDescription");
                                String ord = jsonobject.getString("OrderId");

                                String qty = jsonobject.getString("Quantity");

                                item = new HashMap<String, String>();


                                item.put("line4", jsonobject.getString("Quantity"));
                                item.put("line5", jsonobject.getString("MaterialDescription"));
                                item.put("line1", jsonobject.getString("PartMaterialCode"));
                                //item.put( "line3", jsonobject.getString("MaterialDescription"));
                                item.put("line2", jsonobject.getString("OrderId"));
                                item.put("line3", jsonobject.getString("ProductCode"));
                                list.add(item);

                                Log.v("HELPP", response);

                                Log.v("this is", partcode);
                            }


                            sa = new SimpleAdapter(MaterialSearch.this, list,
                                    R.layout.listofstuff,
                                    new String[]{"line1", "line2", "line3", "line4", "line5"},
                                    new int[]{R.id.line_a, R.id.line_b, R.id.line_c, R.id.line_d, R.id.line_e});

                            ((ListView) findViewById(R.id.listparts)).setAdapter(sa);


//                            ArrayAdapter<String> mArrayAdapter = new ArrayAdapter<String>(MaterialSearch.this,
//                                    android.R.layout.simple_expandable_list_item_1, items);
//                            ListView list = (ListView) findViewById(R.id.warlist);
//                            list.setAdapter(mArrayAdapter);


                            Log.v("HELPP", response);

                            // Toast.makeText(getApplicationContext(), list.toString(), Toast.LENGTH_SHORT).show();
                            // Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();


                        } catch (JSONException e) {

                            Log.e("JSON Parser", "Error parsing data " + e.toString());
                        }

                    }


                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();
            }
        });


        queue.add(strreq);


    }

}