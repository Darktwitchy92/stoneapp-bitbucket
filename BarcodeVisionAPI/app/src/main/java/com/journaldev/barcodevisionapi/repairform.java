package com.journaldev.barcodevisionapi;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import static com.journaldev.barcodevisionapi.Accountsettings.ACCPHONE;
import static com.journaldev.barcodevisionapi.Accountsettings.EMAIL;
import static com.journaldev.barcodevisionapi.Accountsettings.MOBILE;
import static com.journaldev.barcodevisionapi.Accountsettings.NAMETXT;
import static com.journaldev.barcodevisionapi.Accountsettings.SHARED_PREFS6;
import static com.journaldev.barcodevisionapi.login.ETXT;
import static com.journaldev.barcodevisionapi.login.SHARED_PREFSLOG;

public class repairform extends AppCompatActivity {



    private static final int REQUEST_CODE = 1000;
    RequestQueue queue;
    RequestQueue queue2;
    public Spinner menu;
    Spinner secmenu;
    EditText add1;
    EditText postcode;
    EditText conname;
    EditText phone;
    EditText mobile;
    EditText emailtxt;
    EditText boxmsg;
    EditText location;
    EditText town;

    EditText con_name, con_phone, con_email;
    Button locbtn;
    Button sendbtn, con_sendbtn;
    String Serial;
    RadioButton emailbtn;
    RadioButton phonebtn;
    RadioButton confault;
    RadioButton interfault;
    RadioButton contyes;
    RadioButton contno;
    String contype;
    String fstype;
    String sctype;
    String typereq;
    String savename;
    String savephone;
    String flag;
    String defemail;
    String defname;
    String defmob;
    String defphne;
    LinearLayout spinload;





    public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    public static String SHARED_PREFS2 = "shared";
    public static String FTXT = "nothing";
    public static String STXT = "nothing";
    String longitudefloat;
    String latitudefloat;
    FusedLocationProviderClient fusedLocationProviderClient;
    LocationRequest locationRequest;
    LocationCallback locationCallback;

    private static final String TAG = "repairform";

    private DatePickerDialog.OnDateSetListener mdatelisten;
    private TextView mDisplaydate;




    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CODE:



                if (grantResults.length > 0) {

                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {

                    }
                }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scrollrepairform);




        SharedPreferences prefs = getSharedPreferences(SHARED_PREFS6, MODE_PRIVATE);
        String restoredText = prefs.getString(EMAIL, null);
        if (restoredText != null) {
            //  Toast.makeText(this, restoredText, Toast.LENGTH_SHORT).show();
            // defemail = restoredText;
            savephone = prefs.getString(ACCPHONE, null);
            String mobi = prefs.getString(MOBILE, "mob");
            String name = prefs.getString(NAMETXT, null);
            defname = name;
            defmob = mobi;
            defphne = savephone;
            spinload = findViewById(R.id.linspin);
            spinload.setVisibility(View.GONE);
            //Toast.makeText(this, mobi, Toast.LENGTH_SHORT).show();


            final globalvar globvarget = (globalvar) getApplicationContext();

            //defmob = globvarget.getMob();


            SharedPreferences prefs2 = getSharedPreferences(SHARED_PREFSLOG, MODE_PRIVATE);

            defemail = prefs2.getString(ETXT, "EMPTY");


            //Toast.makeText(this, defmob, Toast.LENGTH_SHORT).show();



        }


        Bundle extras = getIntent().getExtras();





        if (TextUtils.isEmpty(defemail)) {

            defemail = " ";
            emailtxt = findViewById(R.id.emailrtext);
            emailtxt.setHint("E-mail");

        }


        menu = (Spinner) findViewById(R.id.requestmenu2);
        secmenu = findViewById(R.id.secondlevel);
        locbtn = (Button) findViewById(R.id.locbtn);
        add1 = findViewById(R.id.addrestxt);
        conname = findViewById(R.id.contatxt);
        phone = findViewById(R.id.phnetxt);
        mobile = findViewById(R.id.mobtxt);
        postcode = findViewById(R.id.pstcode);
        location = findViewById(R.id.loctxt);
        boxmsg = findViewById(R.id.boxtext3);
        sendbtn = findViewById(R.id.sndbtn);
        emailtxt = findViewById(R.id.emailrtext);
        emailbtn = findViewById(R.id.emailradio);
        phonebtn = findViewById(R.id.phoneradio);
        town = findViewById(R.id.twn);
        confault = findViewById(R.id.confault);
        interfault = findViewById(R.id.interfault);
        contyes = findViewById(R.id.yescont);
        contno = findViewById(R.id.nocont);

        con_name = findViewById(R.id.con_name);
        con_phone = findViewById(R.id.con_phone);
        con_email = findViewById(R.id.con_email);

        emailtxt.setText(defemail);
        conname.setText(defname);
        phone.setText(defphne);
        mobile.setText(defmob);


        confault.setVisibility(View.VISIBLE);
        interfault.setVisibility(View.VISIBLE);


        //Toast.makeText(this, defmob, Toast.LENGTH_SHORT).show();


        String serial = extras.getString("serial");


        // Toast.makeText(this, serial, Toast.LENGTH_LONG).show();


       // Toast.makeText(this, typereq, Toast.LENGTH_SHORT).show();

        contno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                conname.setText("");
                phone.setText("");
                mobile.setText("");
                emailtxt.setText("");

                conname.setHint("Contact");
                phone.setHint("Tel");
                mobile.setHint("Mobile");
                emailtxt.setHint("E-mail");




            }
        });

        contyes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                emailtxt.setText(defemail);
                conname.setText(defname);
                phone.setText(defphne);
                mobile.setText(defmob);



            }
        });





        Serial = serial;


        typereq = extras.getString("type");


      //  Toast.makeText(this, typereq, Toast.LENGTH_SHORT).show();
        if (typereq.equals("General")) {



            emailbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (emailbtn.isChecked()) {

                        contype = "E-mail";


                    }

                }
            });

            phonebtn.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

               if (phonebtn.isChecked()) {

           contype = "Phone";


             }

           }

                      });



            mDisplaydate = findViewById(R.id.datetext);


            mDisplaydate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Calendar cal = Calendar.getInstance();
                    int year = cal.get(Calendar.YEAR);
                    int month = cal.get(Calendar.MONTH);
                    int day = cal.get(Calendar.DAY_OF_MONTH);


                    DatePickerDialog dialog = new DatePickerDialog(repairform.this, android.R.style.Widget_Holo,
                            mdatelisten,  day,  month, year);

                    dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);


                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                    dialog.show();

                }
            });

            mdatelisten = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int dayOfMonth, int month, int year) {

                    Log.d(TAG, "dateset:" + dayOfMonth + "/" + "/" + month + "/" + year);

                    month = month + 1;
                    String date =  dayOfMonth + "-" + month + "-" + year;


                    mDisplaydate.setText(date);

                }
            };


            confault.setVisibility(View.GONE);
            interfault.setVisibility(View.GONE);

            final List<String> simplelisthelp = new ArrayList<String>();
            simplelisthelp.add("Please select a fault");
            simplelisthelp.add("Assistance");
            ArrayAdapter<String> spinadapter = new ArrayAdapter<>(repairform.this, R.layout.spinner_item, simplelisthelp);

            menu.setAdapter(spinadapter);

            menu.setSelection(1);


            final List<String> simplelisthelp2 = new ArrayList<String>();
            simplelisthelp2.add("Please select a fault");
            simplelisthelp2.add("General Query");
            ArrayAdapter<String> spinadapter2 = new ArrayAdapter<>(repairform.this, R.layout.spinner_item, simplelisthelp2);

            secmenu.setAdapter(spinadapter2);

            secmenu.setSelection(1);

            menu.setEnabled(false);
            secmenu.setEnabled(false);


            fstype = menu.getSelectedItem().toString();
            sctype = secmenu.getSelectedItem().toString();

            menu.setVisibility(View.GONE);
            secmenu.setVisibility(View.GONE);





      //      Toast.makeText(this, "on help", Toast.LENGTH_SHORT).show();


            flag = "Query";

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
            } else {

                MakelocationRequest();
                Makelocationcallback();


                fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(repairform.this);


                locbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (ActivityCompat.checkSelfPermission(repairform.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                                && ActivityCompat.checkSelfPermission(repairform.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(repairform.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
                            return;
                        }

                        MakelocationRequest();
                        Makelocationcallback();
                        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());


                    }
                });
            }



            sendbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String name = conname.getText().toString();
                    String email = emailtxt.getText().toString();
                    String phonestring = phone.getText().toString();
                    String pcstring = postcode.getText().toString();
                    String address = add1.getText().toString();
                    String descrip = boxmsg.getText().toString();
                    String mob = mobile.getText().toString();
                    String loc = location.getText().toString();
                    String date = mDisplaydate.getText().toString();
                    String city = town.getText().toString();

                    String cleandesc = descrip.replace("'", " ");

                    if (name.length() < 5) {

                        Toast.makeText(repairform.this, "please enter a name", Toast.LENGTH_SHORT).show();
                    } else if (phonestring.length() < 11) {

                        Toast.makeText(repairform.this, "please enter a phone number", Toast.LENGTH_SHORT).show();

                    } else if (address.length() < 9) {

                        Toast.makeText(repairform.this, "please enter your address", Toast.LENGTH_SHORT).show();

                    } else if (!validate(email)) {

                        Toast.makeText(repairform.this, "the email email address typed in is not valid", Toast.LENGTH_LONG).show();

                    } else if (date.equals("")) {

                        Toast.makeText(repairform.this, "Please select a date to contact you", Toast.LENGTH_LONG).show();

                    } else if (menu.getSelectedItemId() == 0) {

                        Toast.makeText(repairform.this, "Please select one of fault types", Toast.LENGTH_LONG).show();

                    } else if (secmenu.getSelectedItemId() == 0) {

                        Toast.makeText(repairform.this, "Please select a second fault type", Toast.LENGTH_LONG).show();

                    } else {

                        sendbtn.setEnabled(false);

                        queue = Volley.newRequestQueue(repairform.this);

                        Bundle extras = getIntent().getExtras();
                        String serial = extras.getString("serial");

                        String capserial = serial.toUpperCase();
                        String mobl = mobile.getText().toString();


                        final String URL = "https://support.stonegroup.co.uk/BarcodeAppPHP/email.php?name=" + name + "&phone=" + phonestring + "&mob=" + mobl + "&addr=" + address +
                                "&pscode=" + pcstring + "&loc=" + loc + "&descp=" + cleandesc + "&email=" + email + "&type=" + typereq + "&date=" + date + "&pref=" + contype + "&serial=" + capserial + "&accmob=" + defmob +
                                "&accphone" + defphne + "&fisrtfault=" + fstype + "&secondfault=" + sctype + "&city=" + city + "&accname=" + defname + "&flag=" + flag + "&accemail=" + defemail;

                        StringRequest strreq = new StringRequest(Request.Method.GET, URL,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        // get response
                                        Log.v("got", response);
                                        //  Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
                                        try {
                                            // Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();

                                            JSONArray jsonarray = new JSONArray(response);

                                            HashMap<String, String> item;
                                            for (int i = 0; i < jsonarray.length(); i++) {

                                                JSONObject jsonobject = jsonarray.getJSONObject(i);
                                                String res = jsonobject.getString("response");
                                                String errcode = jsonobject.getString("errcode");

                                                if (errcode.equals("0")) {
                                                    Toast.makeText(repairform.this, res, Toast.LENGTH_LONG).show();
                                                } else {

                                                    Toast.makeText(repairform.this, "sorry there seems to be an error", Toast.LENGTH_LONG).show();
                                                }
                                            }

                                            Log.v("HELPP", response);

                                        } catch (JSONException e) {

                                            Log.e("JSON Parser", "Error parsing data " + e.toString());
                                        }
                                    }

                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError e) {
                                e.printStackTrace();
                            }
                        });

                        queue.add(strreq);

                        spinload.setVisibility(View.VISIBLE);


                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {


                                Intent tick = new Intent(repairform.this, thankyourepair.class);
                                startActivity(tick);
                            }
                        }, 5000);


                    }
                }
            });


        }else if(typereq.equals("outwar")) {

            String a = "hello this is a out warranty order";

            Toast.makeText(this, a, Toast.LENGTH_SHORT).show();



            emailbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (emailbtn.isChecked()) {

                        contype = "E-mail";


                    }

                }
            });

            phonebtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (phonebtn.isChecked()) {

                        contype = "Phone";


                    }

                }
            });


            confault.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (confault.isChecked()) {

                        flag = "Constant Fault";
                        firstfaultdata();


                    }

                }
            });

            interfault.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (interfault.isChecked()) {

                        flag = "Intermittent Fault";
                        firstfaultdata();


                    }

                }
            });



            mDisplaydate = findViewById(R.id.datetext);


            mDisplaydate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Calendar cal = Calendar.getInstance();
                    int year = cal.get(Calendar.YEAR);
                    int month = cal.get(Calendar.MONTH);
                    int day = cal.get(Calendar.DAY_OF_MONTH);


                    DatePickerDialog dialog = new DatePickerDialog(repairform.this, android.R.style.Widget_Holo,
                            mdatelisten,  day,  month, year);

                    dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);


                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                    dialog.show();

                }
            });

            mdatelisten = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int dayOfMonth, int month, int year) {

                    Log.d(TAG, "dateset:" + dayOfMonth + "/" + "/" + month + "/" + year);

                    month = month + 1;
                    String date =  dayOfMonth + "-" + month + "-" + year;


                    mDisplaydate.setText(date);

                }
            };


            firstfaultdata();


            menu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {



                    ///  Toast.makeText(repairform.this, menu.getSelectedItem().toString(), Toast.LENGTH_LONG).show();

                    SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS2, MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(FTXT, menu.getSelectedItem().toString());

                    editor.commit();


                    fstype = sharedPreferences.getString(FTXT, "empty");
                    secondfaultdata(fstype);


                  //  Toast.makeText(repairform.this, sharedPreferences.getString(FTXT, "empty"), Toast.LENGTH_LONG).show();


                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


            secmenu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {



                    ///  Toast.makeText(repairform.this, menu.getSelectedItem().toString(), Toast.LENGTH_LONG).show();



                    SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS2, MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(STXT, secmenu.getSelectedItem().toString());

                    editor.commit();


                    sctype = sharedPreferences.getString(STXT, "empty");


                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });





           // Toast.makeText(this, "on help", Toast.LENGTH_SHORT).show();


           // flag = "Query";

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
            } else {

                MakelocationRequest();
                Makelocationcallback();


                fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(repairform.this);


                locbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (ActivityCompat.checkSelfPermission(repairform.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                                && ActivityCompat.checkSelfPermission(repairform.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(repairform.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
                            return;
                        }

                        MakelocationRequest();
                        Makelocationcallback();
                        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());


                    }
                });
            }



            sendbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String name = conname.getText().toString();
                    String email = emailtxt.getText().toString();
                    String phonestring = phone.getText().toString();
                    String pcstring = postcode.getText().toString();
                    String address = add1.getText().toString();
                    String descrip = boxmsg.getText().toString();
                    String mob = mobile.getText().toString();
                    String loc = location.getText().toString();
                    String date = mDisplaydate.getText().toString();
                    String city = town.getText().toString();

                    String cleandesc = descrip.replace("'", " ");

                    if (name.length() < 5) {

                        Toast.makeText(repairform.this, "please enter a name", Toast.LENGTH_SHORT).show();
                    } else if (phonestring.length() < 11) {

                        Toast.makeText(repairform.this, "please enter a phone number", Toast.LENGTH_SHORT).show();

                    } else if (address.length() < 9) {

                        Toast.makeText(repairform.this, "please enter your address", Toast.LENGTH_SHORT).show();

                    } else if (!validate(email)) {

                        Toast.makeText(repairform.this, "the email email address typed in is not valid", Toast.LENGTH_LONG).show();

                    } else if (date.equals("")) {

                        Toast.makeText(repairform.this, "Please select a date to contact you", Toast.LENGTH_LONG).show();

                    } else if (menu.getSelectedItemId() == 0) {

                        Toast.makeText(repairform.this, "Please select one of fault types", Toast.LENGTH_LONG).show();

                    } else if (secmenu.getSelectedItemId() == 0) {

                        Toast.makeText(repairform.this, "Please select a second fault type", Toast.LENGTH_LONG).show();

                    } else {

                        sendbtn.setEnabled(false);

                        queue = Volley.newRequestQueue(repairform.this);

                        Bundle extras = getIntent().getExtras();
                        String serial = extras.getString("serial");

                        String capserial = serial.toUpperCase();
                        String mobl = mobile.getText().toString();
                        typereq = "Genreal";


                        final String URL = "https://support.stonegroup.co.uk/BarcodeAppPHP/emailnowar.php?name=" + name + "&phone=" + phonestring + "&mob=" + mobl + "&addr=" + address +
                                "&pscode=" + pcstring + "&loc=" + loc + "&descp=" + cleandesc + "&email=" + email + "&type=" + typereq + "&date=" + date + "&pref=" + contype + "&serial=" + capserial + "&accmob=" + defmob +
                                "&accphone=" + defphne + "&fisrtfault=" + fstype + "&secondfault=" + sctype + "&city=" + city + "&accname=" + defname + "&flag=" + flag + "&accemail=" + defemail;


                        StringRequest strreq = new StringRequest(Request.Method.GET, URL,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        // get response
                                        Log.v("got", response);
                                        //  Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
                                        try {
                                            // Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();

                                            JSONArray jsonarray = new JSONArray(response);

                                            HashMap<String, String> item;
                                            for (int i = 0; i < jsonarray.length(); i++) {

                                                JSONObject jsonobject = jsonarray.getJSONObject(i);
                                                String res = jsonobject.getString("response");
                                                String errcode = jsonobject.getString("errcode");

                                                if (errcode.equals("0")) {
                                                    Toast.makeText(repairform.this, res, Toast.LENGTH_LONG).show();
                                                } else {

                                                    Toast.makeText(repairform.this, "sorry there seems to be an error", Toast.LENGTH_LONG).show();
                                                }
                                            }

                                            Log.v("HELPP", response);

                                        } catch (JSONException e) {

                                            Log.e("JSON Parser", "Error parsing data " + e.toString());
                                        }
                                    }

                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError e) {
                                e.printStackTrace();
                            }
                        });

                        queue.add(strreq);

                        spinload.setVisibility(View.VISIBLE);


                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {


                                Intent tick = new Intent(repairform.this, thankyourepair.class);
                                startActivity(tick);
                            }
                        }, 5000);


                    }
                }
            });

            /// this section is for out of date warranties wanting to buy a new part

        }


        else {


            //   String typehold  = extras.getString("type");


            firstfaultdata();


            menu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {



                    ///  Toast.makeText(repairform.this, menu.getSelectedItem().toString(), Toast.LENGTH_LONG).show();

                    SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS2, MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(FTXT, menu.getSelectedItem().toString());

                    editor.commit();


                    fstype = sharedPreferences.getString(FTXT, "empty");
                    secondfaultdata(fstype);


                   // Toast.makeText(repairform.this, sharedPreferences.getString(FTXT, "empty"), Toast.LENGTH_LONG).show();


                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


            secmenu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {



                    ///  Toast.makeText(repairform.this, menu.getSelectedItem().toString(), Toast.LENGTH_LONG).show();



                    SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS2, MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(STXT, secmenu.getSelectedItem().toString());

                    editor.commit();


                    sctype = sharedPreferences.getString(STXT, "empty");


                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            //   secondfaultdata(menu.getSelectedItem().toString());


            //secondfaultdata();


//        if(typehold.equals("onsite")){
//
//            types = "OnSite Repair";
//        }else if(typehold.equals("rcrepair")){
//
//            types = "Repair Centre Repair";
//
//        }
//        else if(typehold.equals("spear")){
//
//            types = "Spear part order";
//
//
//
//        }


            emailbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (emailbtn.isChecked()) {

                        contype = "E-mail";


                    }

                }
            });

            phonebtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (phonebtn.isChecked()) {

                        contype = "Phone";


                    }

                }
            });


            confault.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (confault.isChecked()) {

                        flag = "Constant Fault";
                        firstfaultdata();


                    }

                }
            });

            interfault.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (interfault.isChecked()) {

                        flag = "Intermittent Fault";
                        firstfaultdata();


                    }

                }
            });


            mDisplaydate = findViewById(R.id.datetext);


            mDisplaydate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Calendar cal = Calendar.getInstance();
                    int year = cal.get(Calendar.YEAR);
                    int month = cal.get(Calendar.MONTH);
                    int day = cal.get(Calendar.DAY_OF_MONTH);


                    DatePickerDialog dialog = new DatePickerDialog(repairform.this, android.R.style.Widget_Holo,
                            mdatelisten, year, month, day);

                    dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);


                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                    dialog.show();

                }
            });

            mdatelisten = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                    Log.d(TAG, "dateset:" + dayOfMonth + "/" + "/" + month + "/" + year);

                    month = month + 1;
                    String date = dayOfMonth + "-" + month + "-" + year ;


                    mDisplaydate.setText(date);

                }
            };


            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
            } else {

                MakelocationRequest();
                Makelocationcallback();


                fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(repairform.this);


                locbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (ActivityCompat.checkSelfPermission(repairform.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                                && ActivityCompat.checkSelfPermission(repairform.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(repairform.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
                            return;
                        }

                        MakelocationRequest();
                        Makelocationcallback();
                        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());


                    }
                });
            }


            // SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS6, MODE_PRIVATE);


            // savephone = sharedPreferences.getString(ACCPHONE, "Empty");
            //savemob = sharedPreferences.getString(MOBILE, "no mob");
            //  savename = sharedPreferences.getString(NAMETXT, "no name");
            // accemail = sharedPreferences.getString(EMAIL, "NO EMAIL FOUND");


            sendbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String name = conname.getText().toString();
                    String email = emailtxt.getText().toString();
                    String phonestring = phone.getText().toString();
                    String pcstring = postcode.getText().toString();
                    String address = add1.getText().toString();
                    String descrip = boxmsg.getText().toString();
                    String mob = mobile.getText().toString();
                    String loc = location.getText().toString();
                    String date = mDisplaydate.getText().toString();
                    String city = town.getText().toString();

                    String cleandesc = descrip.replace("'", " ");

                    if (name.length() < 5) {

                        Toast.makeText(repairform.this, "please enter a name", Toast.LENGTH_SHORT).show();
                    } else if (phonestring.length() < 11) {

                        Toast.makeText(repairform.this, "please enter a phone number", Toast.LENGTH_SHORT).show();

                    } else if (address.length() < 9) {

                        Toast.makeText(repairform.this, "please enter your address", Toast.LENGTH_SHORT).show();

                    } else if (!validate(email)) {

                        Toast.makeText(repairform.this, "the email email address typed in is not valid", Toast.LENGTH_LONG).show();

                    } else if (date.equals("")) {

                        Toast.makeText(repairform.this, "Please select a date to contact you", Toast.LENGTH_LONG).show();

                    } else if (menu.getSelectedItemId() == 0) {

                        Toast.makeText(repairform.this, "Please select one of fault types", Toast.LENGTH_LONG).show();

                    } else if (secmenu.getSelectedItemId() == 0) {

                        Toast.makeText(repairform.this, "Please select a second fault type", Toast.LENGTH_LONG).show();

                    } else {


                        queue = Volley.newRequestQueue(repairform.this);

                        Bundle extras = getIntent().getExtras();
                        String serial = extras.getString("serial");

                        String capserial = serial.toUpperCase();
                        String mobl = mobile.getText().toString();


                        final String URL = "https://support.stonegroup.co.uk/BarcodeAppPHP/email.php?name=" + name + "&phone=" + phonestring + "&mob=" + mobl + "&addr=" + address +
                                "&pscode=" + pcstring + "&loc=" + loc + "&descp=" + cleandesc + "&email=" + email + "&type=" + typereq + "&date=" + date + "&pref=" + contype + "&serial=" + capserial + "&accmob=" + defmob +
                                "&accphone=" + defphne + "&fisrtfault=" + fstype + "&secondfault=" + sctype + "&city=" + city + "&accname=" + defname + "&flag=" + flag + "&accemail=" + defemail;


                        ///flag and set up faults


                        StringRequest strreq = new StringRequest(Request.Method.GET, URL,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        // get response
                                        Log.v("got", response);
                                        //  Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
                                        try {
                                            // Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();

                                            JSONArray jsonarray = new JSONArray(response);

                                            HashMap<String, String> item;
                                            for (int i = 0; i < jsonarray.length(); i++) {

                                                JSONObject jsonobject = jsonarray.getJSONObject(i);
                                                String res = jsonobject.getString("response");
                                                String errcode = jsonobject.getString("errcode");

                                                if (errcode.equals("0")) {
                                                    Toast.makeText(repairform.this, res, Toast.LENGTH_LONG).show();
                                                } else {

                                                    Toast.makeText(repairform.this, "sorry there seems to be an error", Toast.LENGTH_LONG).show();
                                                }
                                            }

                                            Log.v("HELPP", response);

                                        } catch (JSONException e) {

                                            Log.e("JSON Parser", "Error parsing data " + e.toString());
                                        }
                                    }

                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError e) {
                                e.printStackTrace();
                            }
                        });

                        queue.add(strreq);

                        spinload.setVisibility(View.VISIBLE);


                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {


                                Intent tick = new Intent(repairform.this, thankyourepair.class);
                                startActivity(tick);
                            }
                        }, 5000);


                    }
                }
            });


        }
    }


    public void Makelocationcallback() {

        locationCallback = new LocationCallback(){

            @Override
            public void onLocationResult(LocationResult locationResult) {
                for(Location location:locationResult.getLocations()) {


                    longitudefloat = String.valueOf(location.getLongitude());

                    latitudefloat = String.valueOf(location.getLatitude());


                    Geocoder geocoder;
                    List<Address> addresses;





                    geocoder = new Geocoder(repairform.this, Locale.getDefault());




                    try {
                        addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5


                        String address = addresses.get(0).getFeatureName(); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        String cityName = addresses.get(0).getSubAdminArea();
                       // String stateName = addresses.get(0).getAddressLine(1);
                        String countryName = addresses.get(0).getAddressLine(0);
                        String postalCode = addresses.get(0).getPostalCode();
                        String knownName = addresses.get(0).getFeatureName();


                       // Toast.makeText(repairform.this, countryName, Toast.LENGTH_SHORT).show();

                        StringTokenizer tokens = new StringTokenizer(countryName, ",");
                        String add1string = tokens.nextToken();
                        String townhld = tokens.nextToken();

                        StringTokenizer tokens2 = new StringTokenizer(townhld, " ");

                       String acttown = tokens2.nextToken();


                      //  Toast.makeText(repairform.this, acttown, Toast.LENGTH_SHORT).show();






                        add1.setText(add1string);
                        postcode.setText(postalCode);
                       town.setText(acttown);
                     //   Toast.makeText(getApplicationContext(), "set address", Toast.LENGTH_LONG).show();

                        Log.v("address full", addresses.toString());

                   //     Toast.makeText(getApplicationContext(), "set address"+ addresses, Toast.LENGTH_LONG).show();




                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    fusedLocationProviderClient.removeLocationUpdates(locationCallback);



                }



            }
        };


    }

    private void MakelocationRequest() {

        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(5000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setSmallestDisplacement(10);


    }


    public void firstfaultdata() {


        Bundle extras = getIntent().getExtras();
        String serial = extras.getString("serial");


        queue = Volley.newRequestQueue(repairform.this);

        final String URL = "https://support.stonegroup.co.uk/BarcodeAppPHP/faultdata.php?serial="+serial+"&flag="+flag;




        StringRequest strreq = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // get response
                        Log.v("got", response);
                        //  Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
                        try {
                            // Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();

                            JSONArray jsonarray = new JSONArray(response);
                            final List<String> simplelist = new ArrayList<String>();
                            simplelist.add("Please select a fault");
                            HashMap<String, String> item;
                            for (int i = 0; i < jsonarray.length(); i++) {

                                JSONObject jsonobject = jsonarray.getJSONObject(i);
                                String res = jsonobject.getString("FirstLevel");


                                simplelist.add(res);

                            }

                            ArrayAdapter<String> spinadapter = new ArrayAdapter<>(repairform.this, R.layout.spinner_item, simplelist);
                            menu.setAdapter(spinadapter);
                            menu.setSelection(0);










                            Log.v("HELPP", response);

                        } catch (JSONException e) {

                            Log.e("JSON Parser", "Error parsing data " + e.toString());
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();
            }
        });

        queue.add(strreq);







    }


    public void secondfaultdata(String first) {


        Bundle extras = getIntent().getExtras();
        String serial = extras.getString("serial");









        queue2 = Volley.newRequestQueue(repairform.this);








        final String URL = "https://support.stonegroup.co.uk/BarcodeAppPHP/secondfault.php?serial="+serial+"&first="+first+"&flag="+flag;
        final List<String> simplelist2 = new ArrayList<String>();

        simplelist2.add("Please select");

        StringRequest strreqsec = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // get response
                        Log.v("got", response);
                        //  Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
                        try {
                            // Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();

                            JSONArray jsonarray = new JSONArray(response);

                            HashMap<String, String> item;
                            for (int i = 0; i < jsonarray.length(); i++) {

                                JSONObject jsonobject = jsonarray.getJSONObject(i);
                                String res = jsonobject.getString("SecondLevel");


                                simplelist2.add(res);

                            }

                            ArrayAdapter<String> spinadapter = new ArrayAdapter<>(repairform.this, R.layout.spinner_item, simplelist2);
                            secmenu.setAdapter(spinadapter);
                            secmenu.setSelection(0);

                            Log.v("HELPP", response);

                        } catch (JSONException e) {

                            Log.e("JSON Parser", "Error parsing data " + e.toString());
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();
            }
        });

        queue2.add(strreqsec);



    }



    public static boolean validate(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
        return matcher.find();
    }




}
