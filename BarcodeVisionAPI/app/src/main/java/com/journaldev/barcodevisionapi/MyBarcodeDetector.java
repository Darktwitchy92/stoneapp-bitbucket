package com.journaldev.barcodevisionapi;


import android.util.SparseArray;

import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.barcode.Barcode;

class MyBarcodeDetector extends Detector<Barcode> {
    private Detector<Barcode> mDelegate;

    MyBarcodeDetector(Detector<Barcode> delegate) {
        mDelegate = delegate;
    }

    public SparseArray<Barcode> detect(Frame frame) {
        // *** add your custom frame processing code here
        return mDelegate.detect(frame);
    }

    public boolean isOperational() {
        return mDelegate.isOperational();
    }

    @Override


    public boolean setFocus(int id) {
        return mDelegate.setFocus(id);
    }
}
