package com.journaldev.barcodevisionapi;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Locale;

public class Textwelcome extends AppCompatActivity{

    Button continues;
    TextView Header, weblink, lochead, loctext, phonehead, saleshead, suphead, salestxt, suptext;
    ImageView loci, phonei;
    TextView body;
    String numsup, numsale;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if(AppCompatDelegate.getDefaultNightMode()==AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.lightertheme);
        }else setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_text);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);



       Animation animation = AnimationUtils.loadAnimation(Textwelcome.this,R.anim.fadein);

       final Animation B_anime = AnimationUtils.loadAnimation(Textwelcome.this,R.anim.blink_anim);

        Header = findViewById(R.id.tilt);
        body = findViewById(R.id.textbody);
        weblink = findViewById(R.id.weblink);
        lochead = findViewById(R.id.lochead);
        loctext = findViewById(R.id.loctext);
        phonehead = findViewById(R.id.phonehead);
        saleshead = findViewById(R.id.salehed);
        suphead = findViewById(R.id.suphead);
        salestxt = findViewById(R.id.salesphone);
        suptext = findViewById(R.id.supphone);
        phonei = findViewById(R.id.phonicon);





        loci = findViewById(R.id.locimg);
        phonei = findViewById(R.id.phonicon);


        body.setAnimation(animation);
        lochead.setAnimation(animation);
        loctext.setAnimation(animation);

        phonehead.setAnimation(animation);
        saleshead.setAnimation(animation);
        suphead.setAnimation(animation);
        salestxt.setAnimation(animation);
        suptext.setAnimation(animation);
        phonei.setAnimation(animation);
        loci.setAnimation(animation);


        numsup = suptext.toString();
        numsale = salestxt.toString();



        continues = findViewById(R.id.tomain);
        continues.startAnimation(B_anime);
        continues.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                if(login.stat == "logedin"){

                    startActivity(new Intent(Textwelcome.this, MainActivity.class));
                    finish();

                }else {

                    startActivity(new Intent(Textwelcome.this, loginhome.class));
                    finish();
                }

            }
        });


        suptext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                suptext.setAnimation(B_anime);

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:08448802366"));

                startActivity(intent);

            }
        });



        salestxt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                salestxt.setAnimation(B_anime);

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:08448549800"));

                startActivity(intent);

            }
        });


        weblink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {




                weblink.setAnimation(B_anime);

                startActivity(new Intent(Textwelcome.this, stonewebview.class));
                finish();

            }
        });






    }


    @Override
    protected void onResume() {
        super.onResume();
       // suptext.clearAnimation();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration();
        override.locale = new Locale("nl", "NL");
        applyOverrideConfiguration(override);
    }

}
