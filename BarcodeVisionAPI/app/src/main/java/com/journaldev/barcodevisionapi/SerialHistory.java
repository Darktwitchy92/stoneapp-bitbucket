package com.journaldev.barcodevisionapi;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class SerialHistory extends AppCompatActivity {

    ListView seriallist;



    public static String SERIALPREFS = "SERIALS";
    private ArrayList<String> liststuff = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

      setContentView(R.layout.scanhistory);


      seriallist = findViewById(R.id.seriallist);

        final globalvar globvar = (globalvar) getApplicationContext();




            liststuff = globvar.getArrayList(SERIALPREFS);

      if(liststuff.size() == 0){

          ArrayList<String> arr = new ArrayList<>();
          arr.add("empty");
          liststuff = arr;

      }






        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                liststuff );

        seriallist.setAdapter(arrayAdapter);


        seriallist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

               String selection = arrayAdapter.getItem(position).toString();

               if(selection.equals("Empty")){

                   Toast.makeText(globvar, "Sorry, nothing found, Please go back and scan a serialnumber.", Toast.LENGTH_LONG).show();

               }else {


                   Toast.makeText(globvar, selection, Toast.LENGTH_SHORT).show();


                   Intent Intentbundle = new Intent(SerialHistory.this, searchParts.class);
                   Bundle bundle = new Bundle();
                   bundle.putString("data", selection);
                   Intentbundle.putExtras(bundle);
                   startActivity(Intentbundle);

               }
            }
        });


    





    }
}
