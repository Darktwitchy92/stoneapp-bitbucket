package com.journaldev.barcodevisionapi;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.StringTokenizer;

public class Casehistory extends AppCompatActivity {
    RequestQueue queue;
    String Caserefnum;
    String casetype;
    TextView searchcaseop;
    Button searchcase;
    EditText caseent;
    TextView errmsg;
    TextView sbanner;
    String st;
    Drawable icon;



    private SimpleAdapter sa;

    ArrayList<HashMap<String,String>> list = new ArrayList<HashMap<String,String>>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(AppCompatDelegate.getDefaultNightMode()==AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.lightertheme);
        }else setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);


        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.casehistorylay);


        Bundle newstuff = getIntent().getExtras();
        searchcaseop = findViewById(R.id.searchtxt);
        searchcase = findViewById(R.id.searchbtn);
        caseent = findViewById(R.id.caseman);
        errmsg = findViewById(R.id.errmsg);
        sbanner = findViewById(R.id.sbannercd);






        searchcaseop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                searchcaseop.setVisibility(View.GONE);
                searchcase.setVisibility(View.VISIBLE);
                caseent.setVisibility(View.VISIBLE);
                errmsg.setVisibility(View.GONE);
            }
        });

        searchcase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(caseent.length() > 0) {

                    String passover = "";
                    passover = caseent.getText().toString();
                    Intent Intentbundle = new Intent(Casehistory.this, CaseDtl.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("casenum", passover);
                    Intentbundle.putExtras(bundle);
                    startActivity(Intentbundle);
                    errmsg.setVisibility(View.GONE);
                }else{
                    errmsg.setVisibility(View.VISIBLE);
                }

            }
        });






        String account = newstuff.getString("account");
            String seria = newstuff.getString("serial");

            sbanner.setText("SERIAL: " + seria.toUpperCase());


        queue = Volley.newRequestQueue(this);
        final String URL = "https://support.stonegroup.co.uk/BarcodeAppPHP/caseforscaneditem.php?ser="+seria+"&acc="+account;




        StringRequest strreq = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // get response
                        Log.v("got", response);
                        //  Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();


                        try {
                            // Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();

                            JSONArray jsonarray = new JSONArray(response);

                            HashMap<String, String> item;
                            for (int i = 0; i < jsonarray.length(); i++) {

                                JSONObject jsonobject = jsonarray.getJSONObject(i);
                                String acc = jsonobject.getString("AccountCode");
                                String caseref = jsonobject.getString("Case Ref");
                                String repdate = jsonobject.getString("Reported Date");
                                String type = jsonobject.getString("Type");
                                String status = jsonobject.getString("JobStatus");
                                String fault = jsonobject.getString("Fault");
                                //String Typ = jsonobject.getString("Type");
                                //String Stat = jsonobject.getString("Status");
                                //String Resdate = jsonobject.getString("Resolved Date");


                              item = new HashMap<String, String>();


                                item.put("line4", type);
                                item.put("linestat", status);
                                item.put("line5", fault);
                                item.put("line1", acc);
                                //item.put( "line3", jsonobject.getString("MaterialDescription"));
                                item.put("line2", caseref);
                                item.put("line3", repdate);
                                list.add(item);

                                Log.v("HELPP", response);


                                st = status;

                                iconcolor();

                                //Log.v("this is", partcode);

                            }




                            sa = new SimpleAdapter(Casehistory.this, list,
                                    R.layout.listofstuff2,
                                    new String[]{"linestat","line2", "line3", "line4", "line5"},
                                    new int[]{R.id.line_a2,R.id.line_a, R.id.line_b, R.id.line_c, R.id.line_d});

                       ((ListView) findViewById(R.id.caselist)).setAdapter(sa);







                           // ((ListView) findViewById(R.id.caselist)).setTextFilterEnabled(true);

                         ((ListView) findViewById(R.id.caselist)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                public void onItemClick(AdapterView<?> parent, View view,
                                                        int position, long id) {

                                    StringTokenizer tokens = new StringTokenizer(sa.getItem(position).toString(), "=");
                                    String first = tokens.nextToken();// this will contain "Fruit"
                                    String second = tokens.nextToken();// this will contain " they taste good"
                                    String thrid = tokens.nextToken();// this will contain " they taste good"
                                    String fourth = tokens.nextToken();// this will contain " they taste good"
                                    String five = tokens.nextToken().trim();
                                    String six = tokens.nextToken();


                                     five = five.substring(0, five.length() - 7);

                                     Caserefnum = five;


                                    sendser();


                                    Log.v("test5", five);





                                    // String[] arr = {Caserefnum};



                                    // When clicked, show a toast with the TextView text
                                    //Toast.makeText(getApplicationContext(), ((TextView) view).getText(),
                                           // Toast.LENGTH_SHORT).show();
                                }
                            });


//                            ((ListView) findViewById(R.id.caselist)).setOnItemClickListener(new  {
//                                @Override
//                                public void onClick(View v) {
//                                  Caserefnum =  sa.getItem(1).toString();
//                                    Toast.makeText(getApplicationContext(), Caserefnum, Toast.LENGTH_LONG);
//
//                                }
//                            });




//                            ArrayAdapter<String> mArrayAdapter = new ArrayAdapter<String>(MaterialSearch.this,
//                                    android.R.layout.simple_expandable_list_item_1, items);
//                            ListView list = (ListView) findViewById(R.id.warlist);
//                            list.setAdapter(mArrayAdapter);


                            Log.v("HELPP", response);

                            // Toast.makeText(getApplicationContext(), list.toString(), Toast.LENGTH_SHORT).show();
                            // Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();


                        } catch (JSONException e) {

                            Log.e("JSON Parser", "Error parsing data " + e.toString());
                            errmsg.setVisibility(View.VISIBLE);

                        }

                    }


                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();
            }
        });


        queue.add(strreq);


      }
    public void sendser(){
        Intent Intentbundle = new Intent(Casehistory.this, CaseDtl.class);
        Bundle bundle = new Bundle();
        bundle.putString("casenum", Caserefnum);
        bundle.putString("ctype", casetype);
        Intentbundle.putExtras(bundle);
        startActivity(Intentbundle);

    }


    public void iconcolor(){


        if(st.equals("Closed")){

            ImageView i = new ImageView(Casehistory.this);
            i.setImageResource(R.drawable.ic_onlinepngtools);
            DrawableCompat.setTint(
                    i.getDrawable(),
                    ContextCompat.getColor(i.getContext(), R.color.red)
            );






        }else if(st.equals("Open")){

            ImageView i = new ImageView(Casehistory.this);
            i.setImageResource(R.drawable.ic_onlinepngtools);
            DrawableCompat.setTint(
                    i.getDrawable(),
                    ContextCompat.getColor(i.getContext(), R.color.green)
            );

        }else{


            ImageView i = new ImageView(Casehistory.this);
            i.setImageResource(R.drawable.ic_onlinepngtools);
            DrawableCompat.setTint(
                    i.getDrawable(),
                    ContextCompat.getColor(i.getContext(), R.color.orange)
            );
        }

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration();
        override.locale = new Locale("nl", "NL");
        applyOverrideConfiguration(override);
    }




     }



