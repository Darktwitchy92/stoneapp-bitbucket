package com.journaldev.barcodevisionapi;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class newwarrentpage extends AppCompatActivity {


    RequestQueue queue;
    String serialnum;
    String accountnum;
    TextView tv1;
    TextView tv2;
    TextView tv3;
    TextView tv4;
    TextView tv5;
    Button req;

    private SimpleAdapter sa;
    ArrayList<HashMap<String,String>> list = new ArrayList<HashMap<String,String>>();


    protected void onCreate(Bundle savedInstanceState) {
        if(AppCompatDelegate.getDefaultNightMode()==AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.lightertheme);
        }else setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scrollwarandmat);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Bundle newstuff = getIntent().getExtras();

        String partcode = newstuff.getString("Partcode");
        tv1 = findViewById(R.id.tv_1);
        tv2 = findViewById(R.id.tv_2);
        tv3 = findViewById(R.id.tv_3);
        tv4 = findViewById(R.id.tv_4);
        tv5 = findViewById(R.id.tv_5);
        req = findViewById(R.id.reqcall);





        Bundle newser = getIntent().getExtras();

        serialnum = newser.getString("serial");
        accountnum = newser.getString("account");



        req.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(newwarrentpage.this, RequestCall.class);
                startActivity(i);

            }
        });


        queue = Volley.newRequestQueue(this);
        final String URL = "https://support.stonegroup.co.uk/BarcodeAppPHP/partquery.php?serial=" + serialnum + "&acc="+ accountnum;
        final String URL2 = "https://support.stonegroup.co.uk/BarcodeAppPHP/materialSearchquery.php?serial="+serialnum + "&part="+partcode + "&acc=" + accountnum;

        Log.v("serial :", newser.getString("serial"));

        StringRequest strreq = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // get response
                        Log.v("got", response);



                        try {


                            JSONArray jsonarray = new JSONArray(response);


                            for (int i = 0; i < jsonarray.length(); i++) {
                                JSONObject jsonobject = jsonarray.getJSONObject(i);
                                String seria = jsonobject.getString("SerialNumber");
                                String Condes = jsonobject.getString("ContractDescription");
                                String sdate = jsonobject.getString("StartDate");
                                String edate = jsonobject.getString("EndDate");
                                String stat = jsonobject.getString("status");



                                Log.v("HELPP", response);
                            }

                            ArrayList<String> items = new ArrayList<String>();
                            for (int i = 0; i < jsonarray.length(); i++) {
                                JSONObject jsonobject = jsonarray.getJSONObject(i);
                                String seria = jsonobject.getString("SerialNumber");
                                String Condes = jsonobject.getString("ContractDescription");
                                String sdate = jsonobject.getString("StartDate");
                                String edate = jsonobject.getString("EndDate");
                                String stat = jsonobject.getString("status");


                                //DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getApplicationContext());
                                // mTimeText.setText("Time: " + dateFormat.format(date));


                               tv1.setText( seria);
                                tv2.setText("Contract Details: " + Condes);
                                tv3.setText("Your Computer Model: " + seria);
                                tv4.setText("Start Date: " + sdate);
                                tv5.setText("End Date: " + edate);
                                tv5.setText("Status: " + stat);
                                Log.d(seria, "Output");
                            }


                            Log.v("HELPP", response);





//                            ListView list = (ListView) findViewById(R.id.listnew);
//                            // here can be your logic
//                            list.setAdapter(new ArrayAdapter<String>(newwarrentpage.this,
//                                    R.layout.white_text_list, R.id.list_content  ,items));
//                            Log.v("HELPP", response);




                        }



                        catch (JSONException e) {

                            Log.e("JSON Parser", "Error parsing data " + e.toString());
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();
            }
        });




        StringRequest strreq2 = new StringRequest(Request.Method.GET, URL2,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // get response
                        Log.v("got", response);
                        //  Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();




                        try {
                            // Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();

                            JSONArray jsonarray = new JSONArray(response);

                            HashMap<String,String> item;
                            for (int i = 0; i < jsonarray.length(); i++) {

                                JSONObject jsonobject = jsonarray.getJSONObject(i);
                                String partcode = jsonobject.getString("ProductCode");
                                String PartMaterial = jsonobject.getString("PartMaterialCode");
                                String Matdesc = jsonobject.getString("MaterialDescription");
                                String ord = jsonobject.getString("OrderId");

                                String qty = jsonobject.getString("Quantity");

                                item = new HashMap<String,String>();


                                item.put( "line4", jsonobject.getString("Quantity"));
                                item.put( "line5", jsonobject.getString("MaterialDescription"));
                                item.put( "line1", jsonobject.getString("PartMaterialCode"));
                                //item.put( "line3", jsonobject.getString("MaterialDescription"));
                                item.put( "line2", jsonobject.getString("OrderId"));
                                item.put( "line3", jsonobject.getString("ProductCode"));
                                list.add(item);

                                Log.v("HELPP", response);

                                Log.v("this is", partcode);
                            }










                            sa = new SimpleAdapter(newwarrentpage.this, list,
                                    R.layout.listofstuff,
                                    new String[]{ "line1","line2","line3","line4","line5"},
                                    new int[]{R.id.line_a, R.id.line_b,R.id.line_c,R.id.line_d, R.id.line_e});

                            ((ListView)findViewById(R.id.listnew)).setAdapter(sa);


//                            ArrayAdapter<String> mArrayAdapter = new ArrayAdapter<String>(MaterialSearch.this,
//                                    android.R.layout.simple_expandable_list_item_1, items);
//                            ListView list = (ListView) findViewById(R.id.warlist);
//                            list.setAdapter(mArrayAdapter);



                            Log.v("HELPP", response);

                            // Toast.makeText(getApplicationContext(), list.toString(), Toast.LENGTH_SHORT).show();
                            // Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();


                        }






                        catch (JSONException e) {

                            Log.e("JSON Parser", "Error parsing data " + e.toString());
                        }

                    }


                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();
            }
        });





        queue.add(strreq);
        queue.add(strreq2);




    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration();
        override.locale = new Locale("nl", "NL");
        applyOverrideConfiguration(override);
    }
}
