package com.journaldev.barcodevisionapi;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class locationapi extends AppCompatActivity {


    private static final int REQUEST_CODE = 1000;
    TextView txt_location;

    Button btnstart, btnstop;
     String longitudefloat;
    String latitudefloat;



    FusedLocationProviderClient fusedLocationProviderClient;
    LocationRequest locationRequest;
    LocationCallback locationCallback;



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CODE:

                if (grantResults.length > 0) {

                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {

                    }

                }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.locationdemo);



        txt_location = findViewById(R.id.add1);
        btnstart = findViewById(R.id.srt_update);
        btnstop = findViewById(R.id.stp_update);


        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
        } else {

            MakelocationRequest();
            Makelocationcallback();





            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);


            btnstart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ActivityCompat.checkSelfPermission(locationapi.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(locationapi.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(locationapi.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
                        return;
                    }
                    fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());

                    btnstart.setEnabled(!btnstart.isEnabled());
                    btnstop.setEnabled(!btnstop.isEnabled());
                }
            });

            btnstop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (ActivityCompat.checkSelfPermission(locationapi.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(locationapi.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(locationapi.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
                        return;
                    }
                    fusedLocationProviderClient.removeLocationUpdates(locationCallback);


                    btnstart.setEnabled(!btnstart.isEnabled());
                    btnstop.setEnabled(!btnstop.isEnabled());

                }
            });

        }
    }

    public void Makelocationcallback() {

        locationCallback = new LocationCallback(){

            @Override
            public void onLocationResult(LocationResult locationResult) {
               for(Location location:locationResult.getLocations()) {
                   txt_location.setText(String.valueOf(location.getLatitude())
                           + "/" + String.valueOf(location.getLongitude()));

                   longitudefloat = String.valueOf(location.getLongitude());

                   latitudefloat = String.valueOf(location.getLatitude());


                   Geocoder geocoder;
                   List<Address> addresses;





                   geocoder = new Geocoder(locationapi.this, Locale.getDefault());




                   try {
                       addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5


                       String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                       String city = addresses.get(0).getLocality();
                       String state = addresses.get(0).getAdminArea();
                       String country = addresses.get(0).getCountryName();
                       String postalCode = addresses.get(0).getPostalCode();
                       String knownName = addresses.get(0).getFeatureName();

                       txt_location.setText(address + knownName);

                   } catch (IOException e) {
                       e.printStackTrace();
                   }



               }



            }
        };


    }

    private void MakelocationRequest() {

        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(5000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setSmallestDisplacement(10);


    }


}
