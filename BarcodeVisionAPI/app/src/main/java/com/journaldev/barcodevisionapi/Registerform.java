package com.journaldev.barcodevisionapi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.journaldev.barcodevisionapi.Accountsettings.SHARED_PREFS6;


public class Registerform extends AppCompatActivity {

    RequestQueue queue;
    EditText name;
    EditText email;
    EditText phone;
    EditText pass;
    Button sub;


    public static String SHARED_PREFs = "sharedprefs";
    public static String TXT = "nothing";
    public static String PHONETXT = "phone";
    public static String EMAILTXT = "email";
    public static String PASSTXT = "org";



    public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static  final Pattern SPECIAL_CHECK_REGEX = Pattern.compile("[/,:<>!~@#$%*^&()+=?()\"|!\\[#$-]");





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registerformlay);


        name = findViewById(R.id.nametxt);
        email = findViewById(R.id.emailr);
        pass = findViewById(R.id.passwrdtext);
        phone = findViewById(R.id.phonetxt);
        sub = findViewById(R.id.savebtn);



        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if (name.length() < 4) {

                    Toast.makeText(Registerform.this, "  Please enter name more then 4 letters", Toast.LENGTH_LONG).show();

                }else if (validatespecial(name.getText().toString())) {

                    Toast.makeText(Registerform.this, "please remove special Characters from name field", Toast.LENGTH_LONG).show();


                }



                else if (name.length() < 4 && email.length() < 10 && phone.length() < 12 && pass.length() < 9) {

                    Toast.makeText(Registerform.this, "  Please fill in details fully", Toast.LENGTH_LONG).show();

                }else if(pass.length() > 3 && pass.length() < 12) {

                    Toast.makeText(Registerform.this, "password need to be between 6-12 Characters", Toast.LENGTH_LONG).show();

                }else  if (!validate(email.getText().toString())) {

                    Toast.makeText(Registerform.this, "the email email address typed in is not valid", Toast.LENGTH_LONG).show();

                }
                else if(!checkString(pass.getText().toString())){

                    Toast.makeText(Registerform.this, "Please include one captial letter, one number and one special Character.", Toast.LENGTH_LONG).show();


                }

             else if(!validatespecial(pass.getText().toString())){

                    Toast.makeText(Registerform.this, "Please include one special Character.", Toast.LENGTH_LONG).show();

                }else{


                    String strname = name.getText().toString();
                    String stremail = email.getText().toString();
                    String strpass = pass.getText().toString();
                    String strphone = phone.getText().toString();


                    queue = Volley.newRequestQueue(Registerform.this);
                    final String URL = "https://support.stonegroup.co.uk/BarcodeAppPHP/customreg.php?name=" + strname + "&email=" + stremail + "&phone=" + strphone + "&pass=" + strpass;

                    Log.v("saved:", strname);


                    StringRequest strreq = new StringRequest(Request.Method.GET, URL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // get response
                                    Log.v("got", response);
                                    //  Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
                                    try {
                                        // Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();

                                        JSONArray jsonarray = new JSONArray(response);

                                        HashMap<String, String> item;
                                        for (int i = 0; i < jsonarray.length(); i++) {

                                            JSONObject jsonobject = jsonarray.getJSONObject(i);
                                            String res = jsonobject.getString("response");
                                            String errcode = jsonobject.getString("errcode");

                                            if (errcode.equals("1")) {

                                               // Toast.makeText(Registerform.this, res, Toast.LENGTH_SHORT).show();
                                                Toast.makeText(Registerform.this, "Thank you for registering. a confirmation email has been sent to your email address.", Toast.LENGTH_SHORT).show();


                                            }


                                            if (errcode.equals("0")) {

                                                Intent th = new Intent(Registerform.this, Thankyou.class);
                                                startActivity(th);


                                            }

                                        }


//


                                        Log.v("HELPP", response);


                                    } catch (JSONException e) {

                                        Log.e("JSON Parser", "Error parsing data " + e.toString());


                                    }

                                    savedata();


                                }


                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError e) {
                            e.printStackTrace();
                        }
                    });


                    queue.add(strreq);


                }
            }
        });



    }

    public void savedata(){


        String status = "saved";

        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS6, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(TXT, name.getText().toString());
        editor.putString(PHONETXT, phone.getText().toString());
        editor.putString(EMAILTXT, email.getText().toString());
        editor.putString(PASSTXT, pass.getText().toString());


        editor.apply();

        Log.v("data", name.getText().toString());

        String entname = sharedPreferences.getString(TXT, "empty");

        //Toast.makeText(Registerform.this, "saved: "+entname , Toast.LENGTH_LONG).show();

        //textc = sharedPreferences.getString(CHECK, CHECK);








    }

    public static boolean validate(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
        return matcher.find();
    }


    public static boolean validatespecial(String emailStr) {
        Matcher matcher = SPECIAL_CHECK_REGEX.matcher(emailStr);
        return matcher.find();
    }


    private static boolean checkString(String str) {
        char ch;
        boolean capitalFlag = false;
        boolean lowerCaseFlag = false;
        boolean numberFlag = false;
        for(int i=0;i < str.length();i++) {
            ch = str.charAt(i);
            if( Character.isDigit(ch)) {
                numberFlag = true;
            }
            else if (Character.isUpperCase(ch)) {
                capitalFlag = true;
            } else if (Character.isLowerCase(ch)) {
                lowerCaseFlag = true;
            }
            else if(Character.isDigit(ch)){
                numberFlag = true;

            }

            if(numberFlag && capitalFlag && lowerCaseFlag)
                return true;
        }
        return false;
    }



}
