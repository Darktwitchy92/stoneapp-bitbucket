package com.journaldev.barcodevisionapi;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatDelegate;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.Locale;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnTakePicture, btnScanBarcode, gobutton, abtbtn, test, acbtn, signoutbtn;
    TextView textbutton, textbutton2;
    EditText manualtext;
    private Switch myswitch;
    public Dialog dialog;
    private FirebaseAnalytics mFirebaseAnalytics;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(AppCompatDelegate.getDefaultNightMode()==AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.lightertheme);
        }else setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myswitch = findViewById(R.id.sizeswitch);

        if(AppCompatDelegate.getDefaultNightMode()==AppCompatDelegate.MODE_NIGHT_YES){
            myswitch.setChecked(true);
        }
    myswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            if(isChecked){

                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                restartapp();
            }else{

                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                restartapp();
            }

        }
    });
        // ...



        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);



        initViews();
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.METHOD, "login");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, bundle);


        // initiate a Switch



//simpleSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
   // @Override
 ///   public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


//        if(!isChecked){
//            TextView t = (TextView)findViewById(R.id.webtextView);
//           TextView about = (TextView) findViewById(R.id.aboutbtn);
//           TextView scanbutton = (TextView)findViewById(R.id.btnScanBarcode);
//           TextView scanres = (TextView)findViewById(R.id.txtBarcodeValue);
//            textbutton.setTextSize(14);
//            btnScanBarcode.setTextSize(14);
//            btnTakePicture.setTextSize(14);
//            manualtext.setTextSize(14);
//            gobutton.setTextSize(14);
//            t.setTextSize(14);
//            setTheme(R.style.AppTheme);
//            recreate();
//
//
//
//            //scanres.setTextSize(24);
//
//
//            //Toast.makeText(getApplicationContext(),"works", Toast.LENGTH_LONG).show();
//        }else if(isChecked){
//            TextView t = (TextView)findViewById(R.id.webtextView);
//            TextView about = (TextView) findViewById(R.id.aboutbtn);
//            TextView scanbutton = (TextView)findViewById(R.id.btnScanBarcode);
//            TextView scanres = (TextView)findViewById(R.id.txtBarcodeValue);
//            t.setTextSize(20);
//            textbutton.setTextSize(20);
//            btnScanBarcode.setTextSize(20);
//            btnTakePicture.setTextSize(20);
//            manualtext.setTextSize(20);
//            gobutton.setTextSize(20);
//            about.setTextSize(20);
//            scanbutton.setTextSize(16);
//            setTheme(R.style.lightertheme);
//
//            //scanres.setTextSize(30);
//
//            ///Toast.makeText(getApplicationContext(),"works", Toast.LENGTH_LONG).show();
//
//        }



   // }
////});


// check current state of a Switch (true or false).


    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration();
        override.locale = new Locale("nl", "NL");
        applyOverrideConfiguration(override);
    }

    @Override
    public void onBackPressed() {
        minimizeApp();
    }


    public void minimizeApp() {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
    }


    public void restartapp(){

      Intent i = new Intent(getApplicationContext(), MainActivity.class);
      startActivity(i);
      finish();
    }

    private void initViews() {
        btnTakePicture = findViewById(R.id.btnTakePicture);
        btnScanBarcode = findViewById(R.id.btnScanBarcode);
        textbutton = findViewById(R.id.textbutton);
        textbutton2 = findViewById(R.id.textbutton2);
        gobutton = findViewById(R.id.buttongo);
        manualtext = findViewById(R.id.textEDIT);
        abtbtn = findViewById(R.id.aboutbtn);
        test = findViewById(R.id.testbtn);
        acbtn = findViewById(R.id.acsetbtn);
        signoutbtn = findViewById(R.id.signoutbtn);


        gobutton.setOnClickListener(this);
        textbutton.setOnClickListener(this);
        btnTakePicture.setOnClickListener(this);
        btnScanBarcode.setOnClickListener(this);
        textbutton2.setOnClickListener(this);
        abtbtn.setOnClickListener(this);
        test.setOnClickListener(this);
        acbtn.setOnClickListener(this);
        signoutbtn.setOnClickListener(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);


        String instruction = getIntent().getStringExtra("manu");

        if(!TextUtils.isEmpty(instruction)){


            dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
            dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.ic_icons8_info);
            dialog.setContentView(R.layout.dioboxlay);
            dialog.setTitle("Dialog Title");

            dialog.show();



//             settingsDialog = new Dialog(this, R.style.MyAlertDialogStyle);
//            settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//            settingsDialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.ic_icons8_info);
//            settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.dioboxlay
//                    , null));
//
//            settingsDialog.setTitle("How to");
//            settingsDialog.show();
        }





    }



    public void move(View v) {


    }



    public void dismissListener(View v) {


        dialog.hide();
    }



    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.btnTakePicture:
                startActivity(new Intent(MainActivity.this, PictureBarcodeActivity.class));
                break;
            case R.id.btnScanBarcode:
                startActivity(new Intent(MainActivity.this, ScannedBarcodeActivity.class));
                break;
            case R.id.textbutton:
                textbutton.setVisibility(View.GONE);
                manualtext.setVisibility(View.VISIBLE);
                gobutton.setVisibility(View.VISIBLE);

             Animation B_anime = AnimationUtils.loadAnimation(MainActivity.this,R.anim.blink_anim);

                gobutton.setAnimation(B_anime);

                break;



            case R.id.buttongo:
              //  if(manualtext.length() != 0) {





                    String passover = "";
                    passover = manualtext.getText().toString();


                final globalvar globvar = (globalvar) getApplicationContext();

                globvar.addserial(passover);

                    Intent Intentbundle = new Intent(MainActivity.this, searchParts.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("data", passover);
                    Intentbundle.putExtras(bundle);
                    startActivity(Intentbundle);

              //  }else{
                //    manualtext.setVisibility(View.GONE);
                  //  gobutton.setVisibility(View.GONE);
                  //  textbutton2.setVisibility(View.VISIBLE);
                 //   textbutton2.setText("Sorry we could find anything. tap to retry.");
            //    }
                    break;

            case R.id.textbutton2:

                textbutton2.setVisibility(View.GONE);
                textbutton.setVisibility(View.VISIBLE);

                break;


            case R.id.aboutbtn:

                Intent i = new Intent(MainActivity.this, Textwelcome.class);
                startActivity(i);

                break;
            case R.id.testbtn:

                Intent testi = new Intent(this, testbar.class );
                startActivity(testi);

                break;

            case R.id.acsetbtn:

                Intent a = new Intent(MainActivity.this, Accountsettings.class);
                startActivity(a);
                break;

            case R.id.signoutbtn:

                Intent o = new Intent(MainActivity.this, login.class);
                startActivity(o);
                break;




        }




    }






}
