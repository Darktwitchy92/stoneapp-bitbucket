package com.journaldev.barcodevisionapi;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class thankyourepair extends AppCompatActivity {

    RequestQueue queue;
    Button conbtn;
    TextView txtdis;
    TextView casdis;
    TextView casenum;
    String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.thankyoutick);
        final Bundle extra = getIntent().getExtras();
        Intent intent = getIntent();
        casdis = findViewById(R.id.casetext);
        casenum = findViewById(R.id.casenum);

        type = extra.getString("type");


        txtdis = findViewById(R.id.textdis);

        if (type.equals("forgot")) {
            txtdis.setText("Please check you email for your new genrated password");
            conbtn = findViewById(R.id.countbtn);
            casdis.setVisibility(View.GONE);
            casenum.setVisibility(View.GONE);
            conbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(thankyourepair.this, login.class);
                    startActivity(i);
                }
            });

        } else {

            conbtn = findViewById(R.id.countbtn);

            queue = Volley.newRequestQueue(thankyourepair.this);
            final String URL = "https://support.stonegroup.co.uk/BarcodeAppPHP/getcasenum.php";
            StringRequest strreq = new StringRequest(Request.Method.GET, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // get response
                            Log.v("got", response);
                            //  Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
                            try {
                                // Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
                                JSONArray jsonarray = new JSONArray(response);

                                HashMap<String, String> item;
                                for (int i = 0; i < jsonarray.length(); i++) {

                                    JSONObject jsonobject = jsonarray.getJSONObject(i);
                                    String res = jsonobject.getString("res");
                                    String errcode = jsonobject.getString("errcode");
                                    String casenum = jsonobject.getString("casenum");
                                    casdis.setText(casenum);

                                    Toast.makeText(thankyourepair.this, res + casenum, Toast.LENGTH_LONG).show();

                                }

                                Log.v("HELPP", response);

                            } catch (JSONException e) {

                                Log.e("JSON Parser", "Error parsing data " + e.toString());
                                String m = "Sorry either your E-mail or Password is wrong please try again.";
                                Toast.makeText(thankyourepair.this, m, Toast.LENGTH_LONG).show();

                            }

                        }


                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError e) {
                    e.printStackTrace();
                }
            });

            queue.add(strreq);


            conbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Intent i = new Intent(thankyourepair.this, MainActivity.class);
                    startActivity(i);


                }
            });
        }
    }

    public void onBackPressed() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

}
