package com.journaldev.barcodevisionapi;


import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

import static com.journaldev.barcodevisionapi.Accountsettings.SHARED_PREFS6;

public class globalvar extends Application {

    public static final String CHANNEL_ID = "serviceChannel";
    public static String SERIALPREFS = "SERIALS";



        public static String part;
        public static String account;
        public static String name;
        public static String mob;

        public static String MOBI = "mob";
        public static String serial;


    public static ArrayList mySerials = new ArrayList();
    public static HashSet<String> hashSet = new HashSet<String>();

    @Override
    public void onCreate() {
        super.onCreate();



        createnotificationchannel();







    }


    private void createnotificationchannel(){

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel servicechannel = new NotificationChannel(

                    CHANNEL_ID,
                        "Stone Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT

            );

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(servicechannel);
        }

    }







    public String getMob() {

        return mob;
    }

    public void setMob(String aMob) {
        SharedPreferences sha = getSharedPreferences(SHARED_PREFS6, MODE_PRIVATE);
        SharedPreferences.Editor editor = sha.edit();
        mob = aMob;
        editor.putString(MOBI, mob);
        editor.apply();

    }

    public String getName() {

            return name;
        }

        public void setName(String aName) {

            name = aName;

        }

    public String getSerial() {

        return serial;
    }

    public void setSerial(String aSerial) {

        serial = aSerial;

    }

    public String getPart() {

        return part;
    }

    public void setPart(String aPart) {

        part = aPart;

    }

        public String getAccount() {

            return account;
        }

        public void setAccount(String aAccount) {

            account = aAccount;
        }


        public void addserial(String aserial){

            hashSet.addAll(Collections.singleton(aserial));
            //mySerials.clear();
            mySerials.addAll(hashSet);
            saveArrayList(mySerials, SERIALPREFS);


        }

        public ArrayList getserials(){



    //    Toast.makeText(this, getArrayList(SERIALPREFS).toString(), Toast.LENGTH_SHORT).show();

            return mySerials;
        }


    public void saveArrayList(ArrayList<String> list, String key){
        SharedPreferences serialsprefs = PreferenceManager.getDefaultSharedPreferences(globalvar.this);
        SharedPreferences.Editor editor = serialsprefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }

    public ArrayList<String> getArrayList(String key){
        SharedPreferences serialsprefs = PreferenceManager.getDefaultSharedPreferences(globalvar.this);
        Gson gson = new Gson();
        String json = serialsprefs.getString(key, "SERIALS");

        if(json.equals("SERIALS")){

            ArrayList<String> err = new ArrayList<String>();
            err.add("Empty");

            json = err.toString();
        }

        Type type = new TypeToken<ArrayList<String>>() {}.getType();
        return gson.fromJson(json, type);
    }

}
