package com.journaldev.barcodevisionapi;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class newrequestmenu extends AppCompatActivity {

    String ser;

    Button onsitebtn;
    Button spearbtn;
    Button rcbtn;
    Button helpbtn;
    String type;
    String onsite, warstatus;
    String labour;
    String item;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newrequestcallpage);

        Bundle newstuff = getIntent().getExtras();

         ser = newstuff.getString("serial");
         onsite = newstuff.getString("onsite");
            labour = newstuff.getString("labour");
            warstatus = newstuff.getString("warr");
        item = newstuff.getString("items");
        onsitebtn = findViewById(R.id.sitebtn);
        spearbtn = findViewById(R.id.spbtn);
        rcbtn = findViewById(R.id.rcbtn);
        helpbtn = findViewById(R.id.ehbtn);






      //  Toast.makeText(this, item, Toast.LENGTH_SHORT).show();



        if(warstatus.equals("Warranty out dated")){
            onsitebtn.setEnabled(false);
            rcbtn.setEnabled(false);
            spearbtn.setText("Purchase Spare Parts");
            type = "outwar";
           // Toast.makeText(this, type, Toast.LENGTH_SHORT).show();

        }

        if(onsite.equals("0") && labour.equals("0")){
            onsitebtn.setEnabled(false);
            rcbtn.setEnabled(false);

        }

      else if(onsite.equals("1") && labour.equals("0")){
            rcbtn.setEnabled(false);
        }

        else if(onsite.equals("0") && labour.equals("1")){
            onsitebtn.setEnabled(false);
        }






        onsitebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    type = "Visit";
                    repairtype();

            }
        });
        rcbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "Repair Centre";
                repairtype();
            }
        });
        spearbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(warstatus.equals("Warranty out dated")){
                    type = "outwar";
                    repairtype();

                }else {
                    type = "Parts Only";
                    repairtype();
                }
            }
        });

        helpbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "General";
                repairtype();
            }
        });





    }

    void repairtype() {


        Intent i = new Intent(newrequestmenu.this, repairform.class);
        Bundle bundle = new Bundle();
        bundle.putString("serial", ser);
        bundle.putString("type", type);
        //bundle.putString("warr", warstatus);
        i.putExtras(bundle);
        startActivity(i);


    }






}
