package com.journaldev.barcodevisionapi;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Accountsettings extends AppCompatActivity {

    Button savebtn;
    Button chanpass;

    EditText email;
    EditText phone;
    EditText mob;
    EditText orgpass;
    EditText newpass;
    Integer check = 0;
    RequestQueue queue;

  //  String orgphoneatring = login.phondid;
    String curemailadd = login.curemail;
    String message;
    String phoneupdate;
    String mobileupdate;
    String nameacc;




    public static String SHARED_PREFS6 = "sharedprefs3";

    public static String SHARED_PREFSMOB = "sharedprefsmob";
    public static String NAMETXT = "nothing";
    public static String ACCPHONE = "phone";
    public static String MOBILE = "";
    public static String EMAIL = "email";




    public static  final Pattern SPECIAL_CHECK_REGEX = Pattern.compile("[/,:<>!~@#$%^&()+=?()\"|!\\[#$-]");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.accountsettinglay);


        String pass = login.curpass;
       // String id = login.idapp;


        savebtn = findViewById(R.id.saveacbtn);
        chanpass = findViewById(R.id.chpbtn);

        email = findViewById(R.id.emailr);
        phone = findViewById(R.id.phoneacc);
        mob = findViewById(R.id.mobacc);
        newpass = findViewById(R.id.accpass);
        orgpass = findViewById(R.id.orgipass);

        email.setVisibility(View.VISIBLE);
        phone.setVisibility(View.VISIBLE);
        mob.setVisibility(View.VISIBLE);
        newpass.setVisibility(View.GONE);
        orgpass.setVisibility(View.GONE);


        orgpass.setText(pass);

        getdata(curemailadd);



       // phone.setText(orgphoneatring);


        chanpass.setText("Change Password");


        savebtn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {


                String emailstring = email.getText().toString();
                String stringpass;
                String newpassstring = newpass.getText().toString();
                String editphone = phone.getText().toString();
                String mobedit = mob.getText().toString();


                String pass = login.curpass;
                String id = login.idapp;





                if (newpassstring.length() > 3 && newpassstring.length() < 12) {

                    stringpass = newpassstring;
                } else {

                    stringpass = pass;
                }


                if (!checkString(newpass.getText().toString()) && newpass.getText().toString().equals(" ")) {

                    Toast.makeText(Accountsettings.this, "Please include one captial letter, one number and one special Character.", Toast.LENGTH_LONG).show();


                }else if (!validatespecial(newpass.getText().toString()) && newpass.getText().toString().length() > 0) {

                    Toast.makeText(Accountsettings.this, "Please include one special Character.", Toast.LENGTH_LONG).show();

                } else if (newpass.length() < 4 && newpass.length() > 10){

                    Toast.makeText(Accountsettings.this, "password need to be between 4-11 Characters", Toast.LENGTH_LONG).show();

                } else {

                    SharedPreferences sharedPreferencesac2 = getSharedPreferences(SHARED_PREFS6, MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferencesac2.edit();
                    editor.putString(ACCPHONE, editphone);
                    editor.putString(MOBILE, mobedit);

                    editor.putString(EMAIL, emailstring);

                    editor.commit();


                    queue = Volley.newRequestQueue(Accountsettings.this);



                    //editor.putString(MOBILETXT, mobileupdate);












                    final String URL = "https://support.stonegroup.co.uk/BarcodeAppPHP/accountsetting.php?email=" + emailstring + "&pass=" + stringpass + "&id=" + id + "&phone=" + editphone + "&mob=" + mobedit;


                    StringRequest strreq = new StringRequest(Request.Method.GET, URL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // get response
                                    try {


                                        JSONArray jsonarray = new JSONArray(response);


                                        for (int i = 0; i < jsonarray.length(); i++) {

                                            JSONObject jsonobject = jsonarray.getJSONObject(i);
                                            String res = jsonobject.getString("res");
                                            String em = jsonobject.getString("newphone");

                                            message = res;
                                            phoneupdate = em;

                                            Toast.makeText(Accountsettings.this, res, Toast.LENGTH_LONG).show();

                                        }


                                        //phone.setText(phoneupdate);


                                        Log.v("HELPP", response);


                                    } catch (JSONException e) {

                                        Log.e("JSON Parser", "Error parsing data " + e.toString());

                                    }

                                }


                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError e) {
                            e.printStackTrace();
                        }
                    });


                    queue.add(strreq);




                }
            }


        });


        chanpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {





                check++;


                if (check == 1) {
                    email.setVisibility(View.GONE);
                    phone.setVisibility(View.GONE);
                    mob.setVisibility(View.GONE);
                    newpass.setVisibility(View.VISIBLE);
                    orgpass.setVisibility(View.VISIBLE);

                    chanpass.setText("Change details");


                } else if (check == 2) {

                    email.setVisibility(View.VISIBLE);
                    phone.setVisibility(View.VISIBLE);
                    mob.setVisibility(View.VISIBLE);
                    newpass.setVisibility(View.GONE);
                    orgpass.setVisibility(View.GONE);

                    chanpass.setText("Change Password");

                    check = 0;

                }


            }
        });


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

    }


    public void getdata(String em) {




        queue = Volley.newRequestQueue(Accountsettings.this);

        final String URL = "https://support.stonegroup.co.uk/BarcodeAppPHP/getdetails.php?email="+em;


        StringRequest strreq = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            // Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();

                            JSONArray jsonarray = new JSONArray(response);


                            for (int i = 0; i < jsonarray.length(); i++) {

                                JSONObject jsonobject = jsonarray.getJSONObject(i);
                            //    String res = jsonobject.getString("res");
                                String phne= jsonobject.getString("newphone");
                                String emailline = jsonobject.getString("newemail");
                                String mobi = jsonobject.getString("mob");
                                String name = jsonobject.getString("name");

                                message = emailline;
                                phoneupdate = phne;
                                mobileupdate = mobi;
                                nameacc = name;

                                final globalvar globvar = (globalvar) getApplicationContext();





                                SharedPreferences sharedPreferencesac = getSharedPreferences(SHARED_PREFS6, MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferencesac.edit();
                                editor.putString(ACCPHONE, phoneupdate);
                                editor.putString(MOBILE, mobileupdate);
                                editor.putString(NAMETXT, nameacc);
                                editor.putString(EMAIL, message);

                                //editor.putString(MOBILETXT, mobileupdate);










                                editor.apply();
                                email.setText(emailline);
                                phone.setText(phoneupdate);
                                mob.setText(mobileupdate);
                                globvar.setMob(mob.getText().toString());



                                Toast.makeText(Accountsettings.this, mobileupdate, Toast.LENGTH_SHORT).show();


                            }




                            Log.v("HELPP", response);


                        } catch (JSONException e) {

                            Log.e("JSON Parser", "Error parsing data " + e.toString());

                        }

                    }


                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();
            }
        });


        queue.add(strreq);



    }



    public static boolean validatespecial(String emailStr) {
        Matcher matcher = SPECIAL_CHECK_REGEX.matcher(emailStr);
        return matcher.find();
    }


    private static boolean checkString(String str) {
        char ch;
        boolean capitalFlag = false;
        boolean lowerCaseFlag = false;
        boolean numberFlag = false;
        for(int i=0;i < str.length();i++) {
            ch = str.charAt(i);
            if( Character.isDigit(ch)) {
                numberFlag = true;
            }
            else if (Character.isUpperCase(ch)) {
                capitalFlag = true;
            } else if (Character.isLowerCase(ch)) {
                lowerCaseFlag = true;
            }
            else if(Character.isDigit(ch)){
                numberFlag = true;

            }

            if(numberFlag && capitalFlag && lowerCaseFlag)
                return true;
        }
        return false;
    }





}



