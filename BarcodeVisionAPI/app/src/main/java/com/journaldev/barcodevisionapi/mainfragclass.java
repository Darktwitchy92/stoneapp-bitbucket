package com.journaldev.barcodevisionapi;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.FrameLayout;


public class mainfragclass extends AppCompatActivity {


    private BottomNavigationView mnav;
    private FrameLayout mainframe;



    private materialfrag materialp;

    private casefrag casep;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tabframe);


        mainframe = (FrameLayout) findViewById(R.id.m_frame);

        mnav = (BottomNavigationView) findViewById(R.id.m_nav);




        materialp = new materialfrag();
        casep = new casefrag();

        mnav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch(menuItem.getItemId()){



                    case R.id.case_nav:
                        mnav.setItemBackgroundResource(R.color.purple);

                        Intent i = new Intent(mainfragclass.this, CaseDtl.class);
                        startActivity(i);
                        return  true;


                  /*  case R.id.mat_nav:
                        mnav.setItemBackgroundResource(R.color.colorPrimaryDark);

                        return true;*/


                        default:
                            return true;



                }
            }
        });



    }

    private void setfrag(Fragment fragment) {

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.m_frame, fragment);
        fragmentTransaction.commit();


    }


}
