package com.journaldev.barcodevisionapi;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;


public class searchParts extends AppCompatActivity {
    RequestQueue queue;


    String testserial;
    String accountcode;
    public String partcode;
    public static String ser;
    public int count;


    Button statuscolor;
    String res, casecount;
    ImageButton btnn1, btnn2, btnn3;
    public String serialhold;


    public TextView dtl1;
    public TextView dtl2;
    public TextView dt3;
    public TextView statustext;
    public TextView startdatetxt;
    public TextView enddatetxt, badge;

    String onsitestat, warstatus, sessionstat;
    String labourstat;
       private String compcasenum;

    public SimpleAdapter sa;
    ArrayList<HashMap<String,String>> list = new ArrayList<HashMap<String,String>>();
    String[] mStringArray = new String[list.size()];


    private BottomNavigationView mnav;
    private FrameLayout mainframe;




    private materialfrag materialp;

    private casefrag casep;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(AppCompatDelegate.getDefaultNightMode()==AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.lightertheme);
        }else setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scanned_parts);
        // partnum = (TextView)findViewById(R.id.Partnum);
        //contrac = (TextView)findViewById(R.id.contractnum);

        final globalvar globvar = (globalvar) getApplicationContext();



        final Map<String, String> map = new HashMap<String, String>();

        //wrbtn = findViewById(R.id.wrid);
       // matbtn = findViewById(R.id.caid);
      //  caseibtn = findViewById(R.id.caseitembtn);
        dtl1= findViewById(R.id.dtl1);
       dtl2 = findViewById(R.id.tendd2);
      //  dt3 = findViewById(R.id.dtl3);
        statustext = findViewById(R.id.statustxt);
        statuscolor = findViewById(R.id.statussym);
        startdatetxt = findViewById(R.id.tstart);
        enddatetxt = findViewById(R.id.tendd);
        badge = findViewById(R.id.badge_notification_1);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        btnn1 = findViewById(R.id.btn1);
        btnn2 = findViewById(R.id.btn2);
        btnn3 = findViewById(R.id.btn3);

        btnn1.setEnabled(false);
        btnn2.setEnabled(false);
        btnn3.setEnabled(false);


        final TextView errhtv = findViewById(R.id.errh1);
                final TextView errbdytv = findViewById(R.id.errbdy);
                    statuscolor.setEnabled(false);



        Bundle extras = getIntent().getExtras();
        String intentserial = getIntent().getStringExtra("data");

      final TextView tv = findViewById(R.id.sbannercd);

        String data;


        Intent intent = getIntent();
        if(intent.hasExtra("data")){
            data = extras.getString("data");



        }else

            data = intentserial;


        serialhold = data;





        String serhead = "<b>" + "Serial Number: " + "</b>" + data;

       tv.setText(Html.fromHtml(serhead.toUpperCase()));

       ser = data;


        mainframe = findViewById(R.id.m_frame);

        mnav = findViewById(R.id.m_nav);







        btnn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                serialsned_driver();


            }
        });


        btnn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                serialsend();

            }
        });


        btnn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                requestcallfunc();

            }
        });



       // materialp = new materialfrag();
       // casep = new casefrag();







//        mnav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(@NonNull MenuItem Items) {
//
//
//                switch (Items.getItemId()) {
//
//
//
//
//
//
//                    case R.id.mat_nav:
//                        mnav.setItemBackgroundResource(R.color.forestgreen);
//                        mnav.setBackgroundColor(getResources().getColor(R.color.forestgreen));
//                      //   Items.setCheckable(true);
//                        requestcallfunc();
//
//                        break;
//
//                    case R.id.nav_dldrivers:
//                        mnav.setItemBackgroundResource(R.color.babyblue);
//                        mnav.setBackgroundColor(getResources().getColor(R.color.babyblue));
//                        // Items.setCheckable(true);
//                        serialsned_driver();
//
//                        break;
//
//
//                    case R.id.case_nav:
//                        mnav.setItemBackgroundResource(R.color.orange);
//                        mnav.setBackgroundColor(getResources().getColor(R.color.orange));
//                        // Items.setCheckable(true);
//                        // mnav.setSelectedItemId(0);
//
//                        // setfrag(home);
//                        serialsend();
//                        break;
//
//
//                    default:
//                        // Items.setCheckable(true);
//                        break;
//
//
//                }
//
//                return true;
//            }
//
//
//
//        });








        // the request queue
        queue = Volley.newRequestQueue(this);
        final String URL = "https://support.stonegroup.co.uk/BarcodeAppPHP/query.php?serial=";
        //final String URL2 = "http://recycling2.stonegroup.co.uk/BarcodeAppPHP/query.php?";


        final String serialnum = data;

        testserial=data;

        globvar.setSerial(serialnum);

        StringRequest strreq = new StringRequest(Request.Method.GET, URL + serialnum,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // get response
                        Log.v("BOM", response);



                        try {


                            JSONArray jsonarray = new JSONArray(response);


                            for (int i = 0; i < jsonarray.length(); i++) {
                                JSONObject jsonobject = jsonarray.getJSONObject(i);

                                String Part = jsonobject.getString("PartNum");
                                String contr = jsonobject.getString("ContractNumber");
                                String acco = jsonobject.getString("AccountCode");
                                //String session =  jsonobject.getString("session");

                                partcode =  Part;
                                res = response;
                               // sessionstat = session;


                               // Log.v("Session", sessionstat);

                                Log.v("HELPP", response);
                            }


                            ArrayList<String> items = new ArrayList<String>();


                                for (int i = 0; i < jsonarray.length(); i++) {
                                    JSONObject jsonobject = jsonarray.getJSONObject(i);
                                    String Part = jsonobject.getString("PartNum");
                                    String contr = jsonobject.getString("ContractNumber");
                                    String acco = jsonobject.getString("AccountCode");
                                    String sdate = jsonobject.getString("StartDate");
                                    String edate = jsonobject.getString("EndDate");
                                    String onsite = jsonobject.getString("OnSite");
                                    String labour = jsonobject.getString("Labour");
                                    String conttype = jsonobject.getString("ContractDescription");
                                    items.add("Your Computer Model: " + Part);
                                    items.add("Contract number: " + contr);
                                    items.add("Account Code: " + acco);
                                    Log.d(Part, "Output");
                                    Log.d(acco, "Output");
                                    accountcode = acco;
                                    partcode = Part;
                                    onsitestat = onsite;
                                    labourstat = labour;

                                    mStringArray = list.toArray(mStringArray);



                                    globvar.setAccount(acco);
                                    globvar.setPart(Part);

                                    res = contr;
                                    String contrtype = "<b>" + "Warranty Type: " +"</b> "  + conttype;
                                   // String accounthead = "<b>" + "Accont Code: " +"</b> " + acco;
                                    String conhead = "<b>" + "Warranty Number: " +"</b> "  + contr;
                                    String parthead = "<b>" + "Part Number: " +"</b> " + Part;
                                    String starthead = "<b>" + "Warranty Start: " +"</b> " + sdate;
                                    String endhead = "<b>" + "Warranty End: " +"</b> " + edate;

                                        dtl1.setText(Html.fromHtml(parthead));
                                       dtl2.setText(Html.fromHtml(contrtype));
                                       // dt3 .setText(Html.fromHtml(accounthead));
                                    startdatetxt .setText(Html.fromHtml(starthead));
                                    enddatetxt .setText(Html.fromHtml(endhead));





                                }

                            secreq();
                            theridreq();



                        }
                        catch (JSONException e) {

                            Log.e("JSON Parser", "Error parsing data " + e.toString());
                            tv.setText("no record found");
                            //wrbtn.setEnabled(false);
                            //matbtn.setEnabled(false);
                       mnav.setVisibility(View.GONE);
                            errbdytv.setVisibility(View.VISIBLE);
                            errhtv.setVisibility(View.VISIBLE);

                            dtl1.setVisibility(View.GONE);
                          //  dtl2.setVisibility(View.GONE);
                           // dt3.setVisibility(View.GONE);
                            startdatetxt.setVisibility(View.GONE);
                            enddatetxt.setVisibility(View.GONE);


                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {

            if (e instanceof AuthFailureError) {
                tv.setText("ERR");
                    //TODO
                } else if (e instanceof ServerError) {
                tv.setText("ERR");
                    //TODO
                } else if (e instanceof NetworkError) {
                tv.setText("ERR");
                    //TODO
                } else if (e instanceof ParseError) {
                tv.setText("ERR");

                Log.e("JSON Parser", "Error parsing data " + e.toString());
                    //TODO
                }
                e.printStackTrace();
            }
        });





        queue.add(strreq);


        Casecount();




        final Handler handler = new Handler();
        final Timer timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {

                        try {

                                latestreslovedcase(serialhold);

                           // Toast.makeText(searchParts.this, compcasenum, Toast.LENGTH_SHORT).show();


                            if(compcasenum != null){
                                timer.purge();
                                timer.cancel();
                                count = 1;
                                Intent serviceItent = new Intent(searchParts.this, AppService.class);
                                serviceItent.putExtra("data", ser);
                                serviceItent.putExtra("caseExtra", compcasenum);
                                startService(serviceItent);


                               // Toast.makeText(searchParts.this, compcasenum, Toast.LENGTH_SHORT).show();

                            }



                        } catch (Exception e) {
                        }
                    }
                });
            }
        };



            timer.schedule(doAsynchronousTask, 0, 10200); //execute in every 10 minutes




















        // onResume();

       // queue.add(strreq2);





    }




    protected void secreq() {


        final globalvar globvar = (globalvar) getApplicationContext();


        final String Acc = globvar.getAccount();
        final String P = globvar.getPart();
        final String serv = globvar.getSerial();

        final String URL3 = "https://support.stonegroup.co.uk/BarcodeAppPHP/materialSearchquery.php?serial="+serv + "&part="+ P +"&acc="+Acc;



        StringRequest strreq2 = new StringRequest(Request.Method.GET, URL3,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // get response
                        Log.v("got", response);
                        //  Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();




                        try {
                            // Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();

                            JSONArray jsonarray = new JSONArray(response);
                            mStringArray = list.toArray(mStringArray);

                            HashMap<String,String> item;
                            for (int i = 0; i < jsonarray.length(); i++) {

                                JSONObject jsonobject = jsonarray.getJSONObject(i);
                                String partcode = jsonobject.getString("ProductCode");
                                String PartMaterial = jsonobject.getString("PartMaterialCode");
                                String Matdesc = jsonobject.getString("MaterialDescription");
                                String ord = jsonobject.getString("OrderId");


                                String qty = jsonobject.getString("Quantity");

                                item = new HashMap<String,String>();



                                item.put( "line2", "Qty: "+ qty);
                                item.put( "line5", jsonobject.getString("MaterialDescription"));
                                item.put( "line1", jsonobject.getString("PartMaterialCode"));
                                //item.put( "line3", jsonobject.getString("MaterialDescription"));
                              // item.put( "line4", jsonobject.getString("OrderLine"));
                                item.put( "line3", jsonobject.getString("ProductCode"));


                                list.add(item);





                                Log.v("HELPP", response);

                                Log.v("this is", partcode);
                            }






                            Log.v("acc: ", accountcode);
                            Log.v("part: ", partcode);





                            sa = new SimpleAdapter(searchParts.this, list,
                                    R.layout.listofstuff,
                                    new String[]{ "line1","line2","line5"},
                                    new int[]{R.id.line_a, R.id.line_b, R.id.line_C});



                            ((ListView)findViewById(R.id.listnew2)).setAdapter(sa);





//                            ArrayAdapter<String> mArrayAdapter = new ArrayAdapter<String>(MaterialSearch.this,
//                                    android.R.layout.simple_expandable_list_item_1, items);
//                            ListView list = (ListView) findViewById(R.id.warlist);
//                            list.setAdapter(mArrayAdapter);



                            Log.v("HELPP", response);


                            // Toast.makeText(getApplicationContext(), list.toString(), Toast.LENGTH_SHORT).show();
                            // Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();


                        }






                        catch (JSONException e) {

                            Log.e("JSON Parser", "Error parsing data " + e.toString());

                            // Toast.makeText(getApplicationContext(), accountcode + serialnum + partcode, Toast.LENGTH_LONG).show();

                        }



                    }


                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();
            }
        });


        queue.add(strreq2);




    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//
//
//        mnav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(MenuItem menuItem) {
//                switch(menuItem.getItemId()){
//
//
//
//
//
//                    case R.id.case_nav:
//                        mnav.setItemBackgroundResource(R.color.orange);
//                        mnav.setBackgroundColor(getResources().getColor(R.color.orange));
//                        menuItem.setCheckable(false);
//                        // setfrag(home);
//                        serialsend();
//                        break;
//
//
//                   case R.id.mat_nav:
//                        mnav.setItemBackgroundResource(R.color.forestgreen);
//                        mnav.setBackgroundColor(getResources().getColor(R.color.forestgreen));
//                       menuItem.setCheckable(false);
//                       requestcallfunc();
//
//                       break;
//
//                    case R.id.nav_dldrivers:
//                        mnav.setItemBackgroundResource(R.color.babyblue);
//                        mnav.setBackgroundColor(getResources().getColor(R.color.babyblue));
//                        menuItem.setCheckable(false);
//                        serialsned_driver();
//
//                        break;
//
//
//
//                    default:
//                        menuItem.setCheckable(false);
//                        break;
//
//
//
//                }
//
//                return false;
//
//
//            }
//        });
//
//
//
//    }

    protected void theridreq(){

        final globalvar globvar = (globalvar) getApplicationContext();


        final String Acc = globvar.getAccount();
        final String P = globvar.getPart();
        final String serv = globvar.getSerial();

        final String URL = "https://support.stonegroup.co.uk/BarcodeAppPHP/partquery.php?serial=" + serv + "&acc="+ Acc;


        StringRequest strreq = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // get response
                        Log.v("got", response);



                        try {


                            JSONArray jsonarray = new JSONArray(response);


                            for (int i = 0; i < jsonarray.length(); i++) {
                                JSONObject jsonobject = jsonarray.getJSONObject(i);
                                String seria = jsonobject.getString("SerialNumber");
                                String Condes = jsonobject.getString("ContractDescription");
                                String sdate = jsonobject.getString("StartDate");
                                String edate = jsonobject.getString("EndDate");
                                String stat = jsonobject.getString("status");

                                warstatus = stat;



                                Log.v("HELPP", response);
                            }

                            ArrayList<String> items = new ArrayList<String>();
                            for (int i = 0; i < jsonarray.length(); i++) {
                                JSONObject jsonobject = jsonarray.getJSONObject(i);
                                String seria = jsonobject.getString("SerialNumber");
                                String Condes = jsonobject.getString("ContractDescription");
                                String sdate = jsonobject.getString("StartDate");
                                String edate = jsonobject.getString("EndDate");
                                String stat = jsonobject.getString("status");

                                String stathead = "<b>" + "Status: " + "</b> " + stat;

                                statustext.setText(Html.fromHtml(stathead));

                                btnn1.setEnabled(true);
                                btnn2.setEnabled(true);
                                btnn3.setEnabled(true);


                                if(stat.equals("Warranty out dated")) {

                                    mnav.getMenu().getItem(1).setEnabled(false);

                                    Animation blinking = AnimationUtils.loadAnimation(searchParts.this, R.anim.blink_anim);

                                    statuscolor.setBackgroundColor(getResources().getColor(R.color.red));
                                   // statuscolor.startAnimation(blinking);

                                }else{
                                    mnav.getMenu().getItem(1).setEnabled(true);
                                    Animation blinking = AnimationUtils.loadAnimation(searchParts.this, R.anim.blink_anim);
                                    statuscolor.setBackgroundColor(getResources().getColor(R.color.green));
                                   // statuscolor.startAnimation(blinking);
                                }
                               // tv2.setText("Contract Details: " + Condes);
                               // tv3.setText("Your Computer Model: " + seria);
                              //  tv4.setText("Start Date: " + sdate);
                               // tv5.setText("End Date: " + edate);
                              //  tv5.setText("Status: " + stat);
                                Log.d(seria, "Output");
                            }


                            Log.v("HELPP", response);





//                            ListView list = (ListView) findViewById(R.id.listnew);
//                            // here can be your logic
//                            list.setAdapter(new ArrayAdapter<String>(newwarrentpage.this,
//                                    R.layout.white_text_list, R.id.list_content  ,items));
//                            Log.v("HELPP", response);




                        }



                        catch (JSONException e) {

                            Log.e("JSON Parser", "Error parsing data " + e.toString());
                            mnav.setEnabled(false);
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();
            }
        });








        queue.add(strreq);




    }





    public void serialsend(){
        Intent Intentbundle = new Intent(searchParts.this, Casehistory.class);
        Bundle bundle = new Bundle();
        bundle.putString("account", accountcode);
        bundle.putString("serial", testserial);

        Intentbundle.putExtras(bundle);
        startActivity(Intentbundle);



    }


    public void serialsendpart(){
        Intent Intentbundle = new Intent(searchParts.this, newwarrentpage.class);
        Bundle bundle = new Bundle();
        bundle.putString("serial", testserial);
        bundle.putString("account", accountcode);
        bundle.putString("Partcode", partcode);
        Intentbundle.putExtras(bundle);
        startActivity(Intentbundle);

    }


    public void serialsendacc(){
        Intent Intentbundle = new Intent(searchParts.this, warrantycheck.class);
        Bundle bundle = new Bundle();
        bundle.putString("serial", testserial);
        bundle.putString("account", accountcode);
        bundle.putString("Partcode", partcode);
        Intentbundle.putExtras(bundle);
        startActivity(Intentbundle);

    }

    public void serialsned_driver(){
        Intent Intentbundle = new Intent(searchParts.this, emailDriver.class);
        Bundle bundle = new Bundle();
        bundle.putString("serial", testserial);
        Intentbundle.putExtras(bundle);
        startActivity(Intentbundle);

    }

    private void setfrag(Fragment fragment) {

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.m_frame, fragment);
        fragmentTransaction.commit();


    }

    public void requestcallfunc (){

        Intent i = new Intent(searchParts.this, newrequestmenu.class);
        Bundle bundle = new Bundle();
        bundle.putString("serial", testserial);
        bundle.putString("labour", labourstat);
        bundle.putString("onsite", onsitestat);
        bundle.putString("warr", warstatus);

        i.putExtras(bundle);
        startActivity(i);
    }



  /*  public void startService(View v){

        String c = compcasenum;

        Intent serviceItent = new Intent(this, AppService.class);
        serviceItent.putExtra("caseExtra", c);
        startService(serviceItent);


    }*/

    public void stopService(View v){
        Intent serviceItent = new Intent(this, AppService.class);
        stopService(serviceItent);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration();
        override.locale = new Locale("nl", "NL");
        applyOverrideConfiguration(override);
    }



    private void latestreslovedcase(String serial){


        final globalvar globvar = (globalvar) getApplicationContext();


        final String Acc = globvar.getAccount();
        final String P = globvar.getPart();
        final String serv = globvar.getSerial();

        final String URL = "https://support.stonegroup.co.uk/BarcodeAppPHP/latestcasedone.php?ser="+serv;


        StringRequest strreqcase = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // get response
                        Log.v("got", response);



                        try {


                            JSONArray jsonarray = new JSONArray(response);


                            for (int i = 0; i < jsonarray.length(); i++) {
                                JSONObject jsonobject = jsonarray.getJSONObject(i);
                                String casenum = jsonobject.getString("CaseNumber");




                                Log.v("HELPP", response);
                            }

                            ArrayList<String> items = new ArrayList<String>();
                            for (int i = 0; i < jsonarray.length(); i++) {
                                JSONObject jsonobject = jsonarray.getJSONObject(i);
                                String casenum = jsonobject.getString("CaseNumber");

                                compcasenum = casenum;





                            }


                            Log.v("HELPP", response);





                        }



                        catch (JSONException e) {

                            Log.e("JSON Parser", "Error parsing data " + e.toString());
                            mnav.setEnabled(false);
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();
            }
        });


        queue.add(strreqcase);

        //Toast.makeText(searchParts.this, compcasenum, Toast.LENGTH_SHORT).show();


//        if(compcasenum == null){
//
//            Intent serviceItent = new Intent(searchParts.this, AppService.class);
//           stopService(serviceItent);
//
//            notificationcheck = "nonot";
//            count = 0;
//
//
//        }else{
//
//           String c = compcasenum;
//
//           Intent serviceItent = new Intent(searchParts.this, AppService.class);
//            serviceItent.putExtra("caseExtra", c);
//            startService(serviceItent);
//
//            notificationcheck = "show";
//            count++;
//
//        }




    }


    private void Casecount(){


        final globalvar globvar = (globalvar) getApplicationContext();



        final String serv = globvar.getSerial();

        final String URL = "https://support.stonegroup.co.uk/BarcodeAppPHP/casecount.php?ser="+serv;


        StringRequest strreqcasecount = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // get response
                        Log.v("got", response);



                        try {


                            JSONArray jsonarray = new JSONArray(response);


                            for (int i = 0; i < jsonarray.length(); i++) {
                                JSONObject jsonobject = jsonarray.getJSONObject(i);
                                String count = jsonobject.getString("count");

                              //  Toast.makeText(searchParts.this, count, Toast.LENGTH_SHORT).show();

                                casecount = count;
                                badge.setText(casecount);


                                if(casecount.equals("0")){


                                    btnn2.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {


                                            Toast.makeText(searchParts.this, "There is no cases open for this product", Toast.LENGTH_SHORT).show();


                                        }
                                    });


                                }





                                Log.v("HELPP", response);
                            }

                            ArrayList<String> items = new ArrayList<String>();
                            for (int i = 0; i < jsonarray.length(); i++) {
                                JSONObject jsonobject = jsonarray.getJSONObject(i);
                                String count = jsonobject.getString("count");










                            }


                            Log.v("HELPP", response);





                        }


                        catch (JSONException e) {

                            Log.e("JSON Parser", "Error parsing data " + e.toString());
                            mnav.setEnabled(false);
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();
            }
        });


        queue.add(strreqcasecount);





    }






}






