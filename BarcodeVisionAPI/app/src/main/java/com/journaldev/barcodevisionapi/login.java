package com.journaldev.barcodevisionapi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;



public class login extends AppCompatActivity {


    public static String SHARED_PREFSLOG = "sharedprefslog";
    public static String ETXT = "nothing";
    public static String PASSTXT = "phone";
    public static String CHK = "";

    RequestQueue queue;
    EditText email;
    EditText pass;
    Button signbtn;
    TextView fgtbtn;
    ImageButton imgbtn;
    public static String mes;
    public Boolean tick = false;
    CheckBox chk;
    public static String Emailstuff = "";
    public static String stat = "";
    public static String curpass;
    public static String curemail;
    public static String idapp;
    public static String phondid;
    private Switch logswitch;
    public static String emailhod = "";
    public static String passhld = "";
    public FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(AppCompatDelegate.getDefaultNightMode()==AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.lightertheme);
        }else setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loginform);


        email = findViewById(R.id.emailr);
        fgtbtn = findViewById(R.id.forgotbtn);
        pass = findViewById(R.id.newpass);
        signbtn = (Button)findViewById(R.id.signbutton);
        imgbtn = findViewById(R.id.lock);
        chk = findViewById(R.id.checkBox);

        imgbtn.setEnabled(false);









        logswitch = findViewById(R.id.sizeswitchlg);

        if(AppCompatDelegate.getDefaultNightMode()==AppCompatDelegate.MODE_NIGHT_YES){
            logswitch.setChecked(true);
        }
        logswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked){

                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                    restartapp();
                }else{

                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                    restartapp();
                }

            }
        });


        logswitch.setVisibility(View.GONE);

        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFSLOG, MODE_PRIVATE);
          emailhod = sharedPreferences.getString(ETXT, "empty");
          passhld = sharedPreferences.getString(  PASSTXT, "empty");
          String C = sharedPreferences.getString( CHK, "empty");

          Toast.makeText(this, C, Toast.LENGTH_LONG).show();



         if(C.contentEquals("CHKED")){
             email.setText(emailhod);
             pass.setText(passhld);
             Log.d("STUFF", pass.getText().toString());
             chk.setChecked(true);
         }


         fgtbtn.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {



                 Intent forgot = new Intent(login.this, forgotpassword.class);
                 startActivity(forgot);
             }
         });










        signbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if(chk.isChecked()){

                    tick = true;

                    SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFSLOG, MODE_PRIVATE);

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(ETXT, email.getText().toString());
                    editor.putString(PASSTXT, pass.getText().toString());
                    editor.putString(CHK, "CHKED");
                    editor.commit();



                }else{

                    SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFSLOG, MODE_PRIVATE);

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(ETXT, "");
                    editor.putString(PASSTXT, "");
                    editor.putString(CHK, "");
                    editor.commit();
                }



                final String stremail = email.getText().toString();
                    String strpass = pass.getText().toString();


                    queue = Volley.newRequestQueue(login.this);

                    final String URL = "https://support.stonegroup.co.uk/BarcodeAppPHP/login.php?email=" + stremail + "&pass=" + strpass;

                    curpass = strpass;
                    curemail = stremail;

                    Log.v("saved:", email + " : " + pass);





                    StringRequest strreq = new StringRequest(Request.Method.GET, URL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // get response
                                    Log.v("got", response);
                                    //  Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
                                    try {
                                        // Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();

                                        JSONArray jsonarray = new JSONArray(response);

                                        HashMap<String, String> item;
                                        for (int i = 0; i < jsonarray.length(); i++) {

                                            JSONObject jsonobject = jsonarray.getJSONObject(i);
                                            String res = jsonobject.getString("login");
                                            String errcode = jsonobject.getString("errcode");
                                            String id = jsonobject.getString("id");
                                            String curphone = jsonobject.getString("phone");

                                            Toast.makeText(login.this, res, Toast.LENGTH_LONG).show();


                                            idapp = id;
                                            phondid = curphone;
                                            mes = res;


                                            if (errcode.equals("1")) {

                                                Toast.makeText(login.this, res, Toast.LENGTH_LONG).show();
                                                imgbtn.setImageResource(R.drawable.ic_lock);


                                            } else if (errcode.equals("0")) {

                                                Toast.makeText(login.this, res, Toast.LENGTH_LONG).show();
                                                stat = "logedin";
                                                Emailstuff = stremail;
                                                imgbtn.setImageResource(R.drawable.ic_unlock);

                                                Bundle bundle = new Bundle();
                                                bundle.putString(FirebaseAnalytics.Param.METHOD, "login");

                                                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, bundle);


                                                Intent th = new Intent(login.this, MainActivity.class);
                                                startActivity(th);


                                            }
                                            if (errcode.equals("3")) {

                                                Toast.makeText(login.this, res, Toast.LENGTH_LONG).show();

                                            }

                                        }


//


                                        Log.v("HELPP", response);


                                    } catch (JSONException e) {

                                        Log.e("JSON Parser", "Error parsing data " + e.toString());
                                        String m = "Sorry either your E-mail or Password is wrong please try again.";
                                        Toast.makeText(login.this, m, Toast.LENGTH_LONG).show();



                                    }


                                }


                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError e) {
                            e.printStackTrace();
                        }
                    });


                    queue.add(strreq);






            }



});







    }

    public void restartapp(){

        Intent i = new Intent(getApplicationContext(), login.class);
        startActivity(i);
        finish();
    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, loginhome.class);
        startActivity(i);
    }




}
