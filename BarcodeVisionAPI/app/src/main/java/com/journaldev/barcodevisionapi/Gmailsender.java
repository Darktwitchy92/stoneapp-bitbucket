package com.journaldev.barcodevisionapi;


import java.security.Security;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Gmailsender extends javax.mail.Authenticator {
    private String mailhost = "172.16.1.99";
    private String user;
    private String password;
    private Session session;

    static {
        Security.addProvider(new JSSEProvider());
    }

    public Gmailsender(String user) {
        this.user = user;
        this.password = password;

        Properties props = new Properties();


       props.put("mail.smtp.host", mailhost);
        props.put("mail.smtp.port", "25");
        props.put("mail.debug", "true");





//        props.setProperty("mail.transport.protocol", "smtp");
//        props.setProperty("mail.host", mailhost);
//        props.put("mail.smtp.auth", "false");
//        props.put("mail.smtp.port", "25");
//        props.put("mail.smtp.socketFactory.port", "465");
//        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//        props.put("mail.smtp.socketFactory.fallback", "false");
//        props.setProperty("mail.smtp.quitwait", "false");

        //String host ="Stone"+'\\'+ "support.app";
//        Properties props = new Properties();
//       props.put("mail.smtp.host", mailhost);
//        props.put("mail.smtp.auth", "false");
//        props.put("mail.debug", "true");
//        props.put("mail.smtp.port", "25");
       // props.put("mail.transport.protocol", "smtp");


        session = Session.getDefaultInstance(props, this);
    }



    protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(user, password);
    }



    public synchronized void sendMail(String subject, String body,
                                      String sender, String recipients) throws Exception {
        MimeMessage message = new MimeMessage(session);
        DataHandler handler = new DataHandler(new ByteArrayDataSource(body.getBytes(), "text/plain"));
        message.setSender(new InternetAddress(sender));
        message.setSubject(subject);
        message.setDataHandler(handler);


        if (recipients.indexOf(',') > 0)
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipients));
        else
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipients));

        Transport.send(message);
    }


}
