package com.journaldev.barcodevisionapi;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import static com.journaldev.barcodevisionapi.globalvar.CHANNEL_ID;

public class AppService extends Service {


    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        String Casenum = intent.getStringExtra("caseExtra");
        String serial = intent.getStringExtra("data");


        Intent notificationItnet = new Intent(this, searchParts.class);
        notificationItnet.putExtra("data", serial);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationItnet, 0);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Stone Serivce")
                .setContentText("The case "+Casenum+" has been resolved")
                .setSmallIcon(R.drawable.ic_cases)
                .setContentIntent(pendingIntent)
                .build();

        startForeground(1, notification);

        return START_NOT_STICKY;
    }


    @Override
    public void onDestroy()
    {
        super.onDestroy();
        stopSelf();
    }

    @Nullable
@Override
    public IBinder onBind(Intent intent){
        return  null;
    }

}
