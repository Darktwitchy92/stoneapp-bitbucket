package com.journaldev.barcodevisionapi;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class emailDriver extends AppCompatActivity {

    EditText Emailadd;
    EditText serial;
    Button emailbtn;
    String emailaddress;
    String serhold;
    final String URLS = "https://drivers.stonegroup.co.uk/driverfind/index.cfm?searchnum=";
    TextView donemsg;
    final String pass = "96as01013";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.emaildownloadform);





        Bundle extras = getIntent().getExtras();

        String ser = extras.getString("serial");

        serial = (EditText)findViewById(R.id.serialbx);
        Emailadd = (EditText)findViewById(R.id.Emailadd);
        emailbtn = (Button)findViewById(R.id.senddl);
        donemsg = (TextView)findViewById(R.id.sentmsg);


        StringBuilder s = new StringBuilder(100);

       emailaddress = Emailadd.getText().toString();




        serial.setText(ser);
        serhold  = serial.getText().toString();



        String emailsave = login.Emailstuff;


        Emailadd.setText(emailsave);


        emailbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendemail();
                emailbtn.setEnabled(false);
                donemsg.setVisibility(View.VISIBLE);
            }
        });


    }

//    public void sendemail(){
//
//        final String username = "alex.smith@stonegroup.co.uk";
//        final String password = "dark@tw1tchy92";
//
//        Properties props = new Properties();
//        props.put("mail.smtp.starttls.enable", "true");//TLS must be activated
//        props.put("mail.smtp.host", "172.16.1.99");
//        props.put("mail.smtp.auth", "false");
//        props.put("mail.smtp.port", "25");
//
//        Session session = Session.getInstance(props,
//                new javax.mail.Authenticator() {
//                    protected PasswordAuthentication getPasswordAuthentication() {
//                        return new PasswordAuthentication(username, password);
//                    }
//                });
//        try {
//            Message message = new MimeMessage(session);
//            message.setFrom(new InternetAddress("Alex.smith@stonegroup.co.uk"));
//            message.setRecipients(Message.RecipientType.TO,
//                    InternetAddress.parse("alex.smith@stonegroup.co.uk"));
//            message.setSubject("Testing Subject");
//            message.setText("Here is the link to your drivers: " + "\n" + "\n" + URLS+serhold + "\n");
//
//            MimeBodyPart messageBodyPart = new MimeBodyPart();
//
//            Multipart multipart = new MimeMultipart();
//
//            messageBodyPart = new MimeBodyPart();
//            String file = "path of file to be attached";
//            String fileName = "attachmentName";
//            DataSource source = new FileDataSource(file);
//            messageBodyPart.setDataHandler(new DataHandler(source));
//            messageBodyPart.setFileName(fileName);
//            multipart.addBodyPart(messageBodyPart);
//
//            message.setContent(multipart);
//
//            Transport.send(message);
//
//            System.out.println("Done");
//
//        } catch (MessagingException e) {
//            throw new RuntimeException(e);
//        }
//
//    }
//
//
//
//    public boolean isOnline() {
//        ConnectivityManager cm =
//                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo netInfo = cm.getActiveNetworkInfo();
//        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
//            return true;
//        }
//        return false;
//    }


    private void sendemail() {

        new Thread(new Runnable() {

            @Override
            public void run() {

                try {

                    Gmailsender sender = new Gmailsender("Support@stonegroup.co.uk");
                    sender.sendMail("Stone Find Drivers: " + serhold, "Hi," + "\n" + "Thank you for requesting driver information for Serial number: "+ serhold +"." +"\n"  + "Please click below to see the latest drivers for this device ." + "\n" + URLS+serhold  +"\n" +"\n"+ "Thank You.",
                            "Support@stonegroup.co.uk", Emailadd.getText().toString());
                } catch (Exception e) {
                    Log.e("SendMail", e.getMessage(), e);
                }
            }

        }).start();

        Toast toast= Toast.makeText(getApplicationContext(),
                "Thanks, The download link has been sent to your email address.", Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }


}

