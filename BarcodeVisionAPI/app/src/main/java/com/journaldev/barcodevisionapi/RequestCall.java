package com.journaldev.barcodevisionapi;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;

import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


import static android.app.PendingIntent.getActivity;
import static com.journaldev.barcodevisionapi.Accountsettings.EMAIL;
import static com.journaldev.barcodevisionapi.Accountsettings.SHARED_PREFS6;
import static com.journaldev.barcodevisionapi.savedformdata.PHONETXT;
import static com.journaldev.barcodevisionapi.savedformdata.TXT;

public class RequestCall extends AppCompatActivity{

    Spinner menu;
    EditText nameq, emailq, orgq, phoneq, boxtextq, sertxt, postcode, addres, contactname, contactphone, contactorg, conorg, serial;
    Button send, locate;
    int i;
    private static final int REQUEST_CODE = 1000;
    String longitudefloat;
    String latitudefloat;
    FusedLocationProviderClient fusedLocationProviderClient;
    LocationRequest locationRequest;
    LocationCallback locationCallback;
    String textholder;




    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CODE:



                if (grantResults.length > 0) {

                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {

                    }

                }
        }
    }





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.request_call);
        final globalvar globvar = (globalvar) getApplicationContext();
        menu = (Spinner)findViewById(R.id.requestmenu);
        nameq = findViewById(R.id.qname);
        emailq = findViewById(R.id.qemail);
        phoneq = findViewById(R.id.qphone);
        boxtextq = findViewById(R.id.boxtext);
        sertxt = findViewById(R.id.qserial);
        postcode = findViewById(R.id.postc);
        addres = findViewById(R.id.addresso);
        contactname = findViewById(R.id.cname);
        contactphone = findViewById(R.id.contactph);
        contactorg = findViewById(R.id.qorganiz);
        conorg = findViewById(R.id.conorgo);
        serial = findViewById(R.id.qserial);





        locate = findViewById(R.id.locatedata);

        send = findViewById(R.id.btnSendEmail);


        Bundle newstuff = getIntent().getExtras();



        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS6, Context.MODE_PRIVATE);
        String namesave = sharedPreferences.getString(TXT, "");
        String emailsave = sharedPreferences.getString(EMAIL, "");
        String phonesave = sharedPreferences.getString(PHONETXT, "");



        nameq.setText(namesave);
        emailq.setText(emailsave);
        phoneq.setText(phonesave);

        serial.setText(newstuff.getString("serial"));


        Log.v("saveddata:", namesave);

        final List<String> simplelist = new ArrayList<String>();
        simplelist.add("What is your Request About?");
        simplelist.add("Ask a question");
        simplelist.add("Order a part");
        simplelist.add("Book a repair");
        ArrayAdapter<String> spinadapter = new ArrayAdapter<>(this, R.layout.spinner_item, simplelist);
        menu.setAdapter(spinadapter);
        menu.setSelection(0);









        menu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                //Toast.makeText(getApplicationContext(), simplelist.get(position), Toast.LENGTH_LONG).show();
                i = position;
                if(simplelist.get(position) == simplelist.get(1)){

                    Toast.makeText(getApplicationContext(), "question", Toast.LENGTH_LONG).show();

                    nameq.setVisibility(View.VISIBLE);
                    emailq.setVisibility(View.VISIBLE);
                   // orgq.setVisibility(View.VISIBLE);
                    phoneq.setVisibility(View.VISIBLE);
                    boxtextq.setVisibility(View.VISIBLE);
                    sertxt.setVisibility(View.VISIBLE);



                    conorg.setVisibility(View.GONE);

                    postcode.setVisibility(View.GONE);
                    contactname.setVisibility(View.GONE);
                    contactphone.setVisibility(View.GONE);
                    contactorg.setVisibility(View.GONE);

                    addres.setVisibility(View.GONE);

                    locate.setEnabled(false);





                }else if(simplelist.get(position) == simplelist.get(2)){

                    Toast.makeText(getApplicationContext(), "order part", Toast.LENGTH_LONG).show();
                    nameq.setVisibility(View.VISIBLE);
                    emailq.setVisibility(View.VISIBLE);
                   // orgq.setVisibility(View.VISIBLE);
                    phoneq.setVisibility(View.VISIBLE);
                    boxtextq.setVisibility(View.VISIBLE);
                    sertxt.setVisibility(View.VISIBLE);
                    conorg.setVisibility(View.VISIBLE);

                    postcode.setVisibility(View.VISIBLE);
                    contactname.setVisibility(View.VISIBLE);
                    contactphone.setVisibility(View.VISIBLE);
                    contactorg.setVisibility(View.VISIBLE);

                    addres.setVisibility(View.VISIBLE);
                    locate.setEnabled(true);



                }else if(simplelist.get(position) == simplelist.get(3)){

                    Toast.makeText(getApplicationContext(), "book repair", Toast.LENGTH_LONG).show();
                    nameq.setVisibility(View.VISIBLE);
                    emailq.setVisibility(View.VISIBLE);
                   // orgq.setVisibility(View.VISIBLE);
                    phoneq.setVisibility(View.VISIBLE);
                    boxtextq.setVisibility(View.VISIBLE);
                    sertxt.setVisibility(View.VISIBLE);
                    conorg.setVisibility(View.VISIBLE);

                    postcode.setVisibility(View.VISIBLE);
                    contactname.setVisibility(View.VISIBLE);
                    contactphone.setVisibility(View.VISIBLE);
                    contactorg.setVisibility(View.VISIBLE);

                    addres.setVisibility(View.VISIBLE);
                    locate.setEnabled(true);

                }

                else{
                    nameq.setVisibility(View.GONE);
                    emailq.setVisibility(View.GONE);
                    //orgq.setVisibility(View.GONE);
                    phoneq.setVisibility(View.GONE);
                    boxtextq.setVisibility(View.GONE);
                    sertxt.setVisibility(View.GONE);
                    conorg.setVisibility(View.GONE);

                    postcode.setVisibility(View.GONE);
                    contactname.setVisibility(View.GONE);
                    contactphone.setVisibility(View.GONE);
                    contactorg.setVisibility(View.GONE);

                    addres.setVisibility(View.GONE);

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //sets gps location logic for google api


        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
        } else {

            MakelocationRequest();
            Makelocationcallback();





            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(RequestCall.this);


            locate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ActivityCompat.checkSelfPermission(RequestCall.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(RequestCall.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(RequestCall.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
                        return;
                    }

                    MakelocationRequest();
                    Makelocationcallback();
                    fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());

                    //btnstart.setEnabled(!btnstart.isEnabled());
                    //btnstop.setEnabled(!btnstop.isEnabled());



                }
            });


            send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {




                    String name = nameq.getText().toString();
                    String email = emailq.getText().toString();
                    String phone = phoneq.getText().toString();
                    String box = boxtextq.getText().toString();
                    String ser = sertxt.getText().toString();
                    String org = conorg.getText().toString();
                   // String orginization = orgq.getText().toString();
                    String namecontact = contactname.getText().toString();
                    String pstcode = postcode.getText().toString();
                    String contorg = contactorg.getText().toString();
                    String add = addres.getText().toString();
                    String conphne = contactphone.getText().toString();

                    globvar.setName(name);

                    //sends emails depending on what form is filled out


                    if(simplelist.get(i) == simplelist.get(1)){
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("message/rfc822");
                        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"alex.smith@stonegroup.co.uk"});
                        i.putExtra(Intent.EXTRA_SUBJECT, "Customer want's to:  "+simplelist.get(1) + " by " + name + " serialnumber: " + ser);
                        i.putExtra(Intent.EXTRA_TEXT, "Customer Name: "+name + "\n" + "Customer Email: "+email + "\n" + "Customer Phone: "+phone + "\n" + "Organization: "+org + "\n" + "Contact Name: "+namecontact + "\n" + "Customer Postcode: "+ pstcode + "\n" + "Customer Organization: "+contorg + "\n" + "Customer Address: "+add + "\n" + "Contact Phone: "+conphne + "\n"  + "Customer Message: "+ "\n"+box);
                        try {
                            startActivity(Intent.createChooser(i, "Send mail..."));
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(RequestCall.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        send.setEnabled(false);

                    }

                    if(simplelist.get(i) == simplelist.get(2)){
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("message/rfc822");
                        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"alex.smith@stonegroup.co.uk"});
                        i.putExtra(Intent.EXTRA_SUBJECT, "Customer want's to:  "+ simplelist.get(2) + " by " + name + " serialnumber: " + ser);
                        i.putExtra(Intent.EXTRA_TEXT, "Customer Name: "+name + "\n" + "Customer Email: "+email + "\n" + "Customer Phone: "+phone + "\n" + "Organization: "+org + "\n" + "Contact Name: "+namecontact + "\n" + "Customer Postcode: "+ pstcode + "\n" + "Customer Organization: "+contorg + "\n" + "Customer Address: "+add + "\n" + "Contact Phone: "+conphne + "\n"  + "Customer Message:  "+ "\n"+box);
                        try {
                            startActivity(Intent.createChooser(i, "Send mail..."));
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(RequestCall.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();

                        }
//conformation email

                        new Thread(new Runnable() {

                            @Override
                            public void run() {
                               String n = globvar.getName();
                                try {
                                    Gmailsender sender = new Gmailsender("alexlloyd.smith92@gmail.com");
                                    sender.sendMail("Hello from JavaMail", "Your Request has been sent " + n,
                                            "alexlloyd.smith92@gmail.com", "alex.smith@stonegroup.co.uk");
                                } catch (Exception e) {
                                    Log.e("SendMail", e.getMessage(), e);
                                }
                            }

                        }).start();
                    }else{
                        send.setEnabled(false);
                    }

                    if(simplelist.get(i) == simplelist.get(3)){
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("message/rfc822");
                        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"alex.smith@stonegroup.co.uk"});
                        i.putExtra(Intent.EXTRA_SUBJECT, "Customer want's to:  "+simplelist.get(3) + " by " + name + " serialnumber: " + ser);
                        i.putExtra(Intent.EXTRA_TEXT, name + "\n" + email + "\n" + phone + "\n" + "\n" + box);
                        try {
                            startActivity(Intent.createChooser(i, "Send mail..."));
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(RequestCall.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        send.setEnabled(false);


                    }




                }
            });


        }



    }

    public void Makelocationcallback() {

        locationCallback = new LocationCallback(){

            @Override
            public void onLocationResult(LocationResult locationResult) {
                for(Location location:locationResult.getLocations()) {


                    longitudefloat = String.valueOf(location.getLongitude());

                    latitudefloat = String.valueOf(location.getLatitude());


                    Geocoder geocoder;
                    List<Address> addresses;





                    geocoder = new Geocoder(RequestCall.this, Locale.getDefault());




                    try {
                        addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5


                        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        String city = addresses.get(0).getLocality();
                        String state = addresses.get(0).getAdminArea();
                        String country = addresses.get(0).getCountryName();
                        String postalCode = addresses.get(0).getPostalCode();
                        String knownName = addresses.get(0).getFeatureName();

                        addres.setText( address);
                        postcode.setText(postalCode);
                        Toast.makeText(getApplicationContext(), "set address", Toast.LENGTH_LONG).show();


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    fusedLocationProviderClient.removeLocationUpdates(locationCallback);



                }



            }
        };


    }

    private void MakelocationRequest() {

        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(5000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setSmallestDisplacement(10);


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration();
        override.locale = new Locale("nl", "NL");
        applyOverrideConfiguration(override);
    }




}
