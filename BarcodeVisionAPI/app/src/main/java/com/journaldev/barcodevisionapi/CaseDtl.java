package com.journaldev.barcodevisionapi;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.method.ScrollingMovementMethod;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;


public class CaseDtl extends AppCompatActivity {


    RequestQueue queue;

    TextView descrr;
    TextView errr;
    TextView serialban;


    String Casenum;
    String casetype;







    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(AppCompatDelegate.getDefaultNightMode()==AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.lightertheme);
        }else setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);


        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.warranty_dtl);
        Bundle newser = getIntent().getExtras();


        descrr = findViewById(R.id.descrip);

        descrr.setMovementMethod(new ScrollingMovementMethod());

        Casenum = newser.getString("casenum");
        casetype = newser.getString("ctype");

        errr = findViewById(R.id.errmsgdtl);
        serialban = findViewById(R.id.sbannercd);




        queue = Volley.newRequestQueue(this);
        final String URL = "https://support.stonegroup.co.uk/BarcodeAppPHP/Casedtl.php?cc="+Casenum;







        StringRequest strreq = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // get response
                        Log.v("got", response);
                       // Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();


                        try {
                           // Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();

                            JSONArray jsonarray = new JSONArray(response);



                            for (int i = 0; i < jsonarray.length(); i++) {
                                JSONObject jsonobject = jsonarray.getJSONObject(i);
                                String cnum = jsonobject.getString("1");
                                String report = jsonobject.getString("2");
                                String status = jsonobject.getString("3");
                                String case_type = jsonobject.getString("4");
                                String serialnum = jsonobject.getString("5");
                                String produ = jsonobject.getString("6");
                                String flag = jsonobject.getString("7");
                                String flevel = jsonobject.getString("8");
                                String fslevel = jsonobject.getString("9");
                                String repobyf = jsonobject.getString("10");
                                String contac = jsonobject.getString("11");
                                String fonenum = jsonobject.getString("12");
                                String mobnum = jsonobject.getString("13");
                                String Emailadd = jsonobject.getString("14");
                                String address1 = jsonobject.getString("15");
                                String address2 = jsonobject.getString("16");
                                String address3 = jsonobject.getString("17");
                                String address4 = jsonobject.getString("18");
                                String address5 = jsonobject.getString("19");
                                String postcod = jsonobject.getString("20");
                                String customref = jsonobject.getString("21");
                                String descr = jsonobject.getString("22");
                                String partnum = jsonobject.getString("23");
                                String ordnum = jsonobject.getString("24");
                                String contract = jsonobject.getString("25");
                                String exp = jsonobject.getString("26");




                                descrr.setText("Description: " + descr);









                                Log.v("HELPP", cnum);
                            }







                            ArrayList<String> items = new ArrayList<String>();
                            ArrayList<String> items2 = new ArrayList<String>();
                           for (int i = 0; i < jsonarray.length(); i++) {
                               JSONObject jsonobject = jsonarray.getJSONObject(i);
                               String cnum = jsonobject.getString("1");
                               String report = jsonobject.getString("2");
                               String status = jsonobject.getString("3");
                               String case_type = jsonobject.getString("4");
                               String serialnum = jsonobject.getString("5");
                               String produ = jsonobject.getString("6");
                               String flag = jsonobject.getString("7");
                               String flevel = jsonobject.getString("8");
                               String fslevel = jsonobject.getString("9");
                               String repobyf = jsonobject.getString("10");
                               String contac = jsonobject.getString("11");
                               String fonenum = jsonobject.getString("12");
                               String mobnum = jsonobject.getString("13");
                               String Emailadd = jsonobject.getString("14");
                               String address1 = jsonobject.getString("15");
                               String address2 = jsonobject.getString("16");
                               String address3 = jsonobject.getString("17");
                               String address4 = jsonobject.getString("18");
                               String address5 = jsonobject.getString("19");
                               String postcod = jsonobject.getString("20");
                               String customref = jsonobject.getString("21");
                               String descr = jsonobject.getString("22");
                               String partnum = jsonobject.getString("23");
                               String ordnum = jsonobject.getString("24");
                               String contract = jsonobject.getString("25");
                               String exp = jsonobject.getString("26");
                               String jobnum = jsonobject.getString("jobnum");
                               String jobschd = jsonobject.getString("ScheduleDate");
                               String engineer = jsonobject.getString("CaseSkill");
                               String parts = jsonobject.getString("parts");
                               String partshipped = jsonobject.getString("partshipped");
                               String defectcode = jsonobject.getString("DefectCode");
                               String repaircode = jsonobject.getString("RepairCode");
                               String usedparts = jsonobject.getString("usedparts");
                               String jobcomp = jsonobject.getString("jobcompdate");
                               String shipdate = jsonobject.getString("Shipdate");



                               if(case_type.equals("Visit")){


//
//
                                items.add("Case Ref: " + cnum);
                                // items.add("Job Number: " +jobnum);
                                items.add("Reported: " + report);
                                items.add("Status: " + status);
                                items.add("Case Type: " + case_type);
                                items.add("Serial Number: " + serialnum);
                                items.add("Product: " + produ);
                                items.add("Fault: " + flag + " - " + flevel + " - " + fslevel);
                                items.add("Schedule Date:  " + jobschd);
                                items2.add("Defect Code: " + defectcode);
                                items2.add("Repair Code: " + repaircode);
                                items2.add("Parts Used: " + usedparts);

//////Check this out. filter for list parts only
                            }if(case_type.equals("Repair Centre")) {


//
//
                                   items.add("Case Ref: " + cnum);
                                   // items.add("Job Number: " +jobnum);
                                   items.add("Reported: " + report);
                                   items.add("Status: " + status);
                                   items.add("Case Type: " + case_type);
                                   items.add("Serial Number: " + serialnum);
                                   items.add("Product: " + produ);
                                   items.add("Fault: " + flag + " - " + flevel + " - " + fslevel);
                                   items.add("Schedule Date: " + jobschd);
                                   items2.add("Defect Code: " + defectcode);
                                   items2.add("Repair Code: " + repaircode);
                                   items2.add("Job Complete Date: " + jobcomp);
                                   items2.add("Shipping Date: " + shipdate);


//////Check this out. filter for list parts only
                               }if(case_type.equals("Parts Only")){


                                final SpannableStringBuilder sb = new SpannableStringBuilder("Parts:");
                                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
                                sb.setSpan(bss, 0, 4, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

                                items.add("Case Ref: " + cnum);
                                // items.add("Job Number: " +jobnum);
                                items.add("Reported: " + report);
                                items.add("Status: " + status);
                                items.add("Case Type: " + case_type);
                                items.add("Serial Number: " + serialnum);
                                items.add("Product: " + produ);
                                items.add("Fault: " + flag + " - " + flevel + " - " + fslevel);
                                   items2.add(sb + usedparts);
                                items2.add("Parts Shipped: " + partshipped);





                            }




                             //  items.add("Contract: "+contract);

                               descrr.setText(descr);
                               serialban.setText("Serial: " + serialnum);





                           }
                            ListView list = (ListView)findViewById(R.id.dtllist);


                            list.setAdapter(new ArrayAdapter<String>(CaseDtl.this,
                                    R.layout.whitetext2, R.id.list_content2  ,items));



                            ListView list2 = (ListView)findViewById(R.id.newextra);




                            list2.setAdapter(new ArrayAdapter<String>(CaseDtl.this,
                                    R.layout.whitetext2, R.id.list_content2  ,items2));







                            Log.v("HELPP", response);

                            Log.v("HELPP", response);




                        }



                        catch (JSONException e) {

                            Log.e("JSON Parser", "Error parsing data " + e.toString());
                            errr.setVisibility(View.VISIBLE);
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();
            }
        });




        queue.add(strreq);






    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration();
        override.locale = new Locale("nl", "NL");
        applyOverrideConfiguration(override);
    }



}

