package com.journaldev.barcodevisionapi;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class warrantycheck extends AppCompatActivity{



    RequestQueue queue;
    String serialnum;
    String accountnum;
    TextView errmesgbox;


@Override
protected void onCreate(Bundle savedInstanceState) {
    if(AppCompatDelegate.getDefaultNightMode()==AppCompatDelegate.MODE_NIGHT_YES){
        setTheme(R.style.lightertheme);
    }else setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);


    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
    setContentView(R.layout.warr_layout);

    Bundle newser = getIntent().getExtras();

    serialnum = newser.getString("serial");
    accountnum = newser.getString("account");
    errmesgbox = findViewById(R.id.errmsgitem);



    queue = Volley.newRequestQueue(this);
    final String URL = "https://support.stonegroup.co.uk/BarcodeAppPHP/caseforscaneditem.php?ser=" + serialnum + "&acc="+ accountnum;

    Log.v("serial :", newser.getString("serial"));



    StringRequest strreq = new StringRequest(Request.Method.GET, URL,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    // get response
                    Log.v("got", response);
                   // Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();


                    try {
                        Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();

                        JSONArray jsonarray = new JSONArray(response);


                        for (int i = 0; i < jsonarray.length(); i++) {
                            JSONObject jsonobject = jsonarray.getJSONObject(i);
                            String seria = jsonobject.getString("AccountCode");
                            String Condes = jsonobject.getString("Case Ref");
                            String sdate = jsonobject.getString("Reported Date");
                            String edate = jsonobject.getString("Product");
                            String stat = jsonobject.getString("Fault");
                            String typ = jsonobject.getString("Type");
                            String sta = jsonobject.getString("Status");
                            String resdate = jsonobject.getString("Resolved Date");



                            Log.v("HELPP", response);
                        }

                        ArrayList<String> items = new ArrayList<String>();
                        for (int i = 0; i < jsonarray.length(); i++) {
                            JSONObject jsonobject = jsonarray.getJSONObject(i);
                            String acc = jsonobject.getString("AccountCode");
                            String casref = jsonobject.getString("Case Ref");
                            String rdate = jsonobject.getString("Reported Date");
                            String prod = jsonobject.getString("Product");
                            String faul = jsonobject.getString("Fault");
                            String typ = jsonobject.getString("Type");
                            String sta = jsonobject.getString("Status");
                            String resdate = jsonobject.getString("Resolved Date");

                            //DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getApplicationContext());
                           // mTimeText.setText("Time: " + dateFormat.format(date));


                            items.add("Your Account Code: " + acc);
                            items.add("Case Ref: " + casref);
                            items.add("Reported Date: " + rdate);
                            items.add("Product: " + prod);
                            items.add("Fault: " + faul);
                            items.add("Type: " + typ);
                            items.add("Status: "+ sta);
                            items.add("Resolved Date: "+ resdate);
                            Log.d(acc, "Output");
                        }


                        Log.v("HELPP", response);


                        ListView list = (ListView) findViewById(R.id.warlist);
                        // here can be your logic
                        list.setAdapter(new ArrayAdapter<String>(warrantycheck.this,
                                R.layout.white_text_list, R.id.list_content  ,items));
                        Log.v("HELPP", response);

                      //  Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();


                    }



                    catch (JSONException e) {

                        Log.e("JSON Parser", "Error parsing data " + e.toString());
                        errmesgbox.setVisibility(View.VISIBLE);
                    }

                }
            }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError e) {
            e.printStackTrace();
        }
    });




    queue.add(strreq);




    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration();
        override.locale = new Locale("nl", "NL");
        applyOverrideConfiguration(override);
    }
}
