package com.journaldev.barcodevisionapi;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Camera;
import android.graphics.SurfaceTexture;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.view.SurfaceHolder;
import android.view.TextureView;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;


import java.io.IOException;

import static com.google.android.gms.vision.CameraSource.CAMERA_FACING_BACK;


public class testbar extends AppCompatActivity {


    private TextureView mtextureview;
    private CameraSource cameraSource;
    private Camera mCamera;
    private SurfaceHolder shold;
    private BarcodeDetector   mybarcodeDetector;
    private static final int REQUEST_CAMERA_PERMISSION = 201;
    private SurfaceTexture suref;
    private TextureView.SurfaceTextureListener msurfacelistener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {

            Toast.makeText(getApplicationContext(), "there is a view", Toast.LENGTH_SHORT).show();
            try {
                if (ActivityCompat.checkSelfPermission(testbar.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {



                    cameraSource.start();
                } else {
                    ActivityCompat.requestPermissions(testbar.this, new
                            String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {

        }
    };



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.textureviewtest);

      //  mtextureview = (TextureView)findViewById(R.id.textureView);


        suref = mtextureview.getSurfaceTexture();

        mybarcodeDetector  = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.ISBN)
                .build();



    }

    @Override
    protected void onResume() {
        super.onResume();



        if(mtextureview.isAvailable()){


            cameraSource = new CameraSource.Builder(this.mtextureview.getContext(), mybarcodeDetector)
                    .setRequestedPreviewSize(1280, 720)

                    .setFacing(CAMERA_FACING_BACK )

                    .build();


        }else{
            mtextureview.setSurfaceTextureListener(msurfacelistener);
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        View decorView = getWindow().getDecorView();
        if(hasFocus){

            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            |View.SYSTEM_UI_FLAG_FULLSCREEN
            |View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);

        }
    }
}




