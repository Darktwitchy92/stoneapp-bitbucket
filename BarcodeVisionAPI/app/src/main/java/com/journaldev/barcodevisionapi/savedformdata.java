package com.journaldev.barcodevisionapi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class savedformdata extends Textwelcome {


    EditText nametext;
    EditText phonetext;
    EditText emailtext;
    EditText orgtext;
    EditText orgcontxt;
    EditText orgphonetxt;
    Button savedbtn;

    String textc;

    public static String SHARED_PREFS4 = "sharedprefs";
    public static String TXT = "nothing";
    public static String PHONETXT = "phone";
    public static String EMAILTXT = "email";
    public static String ORGTXT = "org";
    public static String ORGCONTXT = "orgcon";
    public static String ORGPHONETXT = "orgtxt";

    public static String CHECK = "CHECK";






    protected void onCreate(Bundle savedInstanceState) {
        if(textc == "saved"){

            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);


        }



        super.onCreate(savedInstanceState);
        setContentView(R.layout.saveformlayout);






        nametext = findViewById(R.id.nametxt);
        phonetext = findViewById(R.id.phonetxt);
        emailtext = findViewById(R.id.emailr);
        orgtext = findViewById(R.id.passwrdtext);
        orgcontxt = findViewById(R.id.orgcon);
        orgphonetxt = findViewById(R.id.orgphone);



        savedbtn = (Button) findViewById(R.id.savebtn);


        savedbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                savedata();
                Intent ma = new Intent(savedformdata.this, MainActivity.class);
                startActivity(ma);


            }
        });

    }


    public void savedata(){


        String status = "saved";

        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS4, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(TXT, nametext.getText().toString());
        editor.putString(PHONETXT, phonetext.getText().toString());
        editor.putString(EMAILTXT, emailtext.getText().toString());
        editor.putString(ORGTXT, orgtext.getText().toString());
        editor.putString(ORGCONTXT, orgcontxt.getText().toString());
        editor.putString(ORGPHONETXT, orgphonetxt.getText().toString());
        editor.putString(CHECK, status);

        editor.commit();

        Log.v("data", nametext.getText().toString());

        String entname = sharedPreferences.getString(TXT, "empty");

     Toast.makeText(savedformdata.this, "saved: "+entname , Toast.LENGTH_LONG).show();

        //textc = sharedPreferences.getString(CHECK, CHECK);






    }


}
