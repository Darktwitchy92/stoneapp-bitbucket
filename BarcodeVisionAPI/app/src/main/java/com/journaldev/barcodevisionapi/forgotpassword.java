package com.journaldev.barcodevisionapi;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class forgotpassword extends AppCompatActivity {
    EditText email;
    Button cont;
    String page;
    RequestQueue queue;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgotpasslay);


        email = findViewById(R.id.emailforgot);
        cont = findViewById(R.id.fcontbtn);



        cont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                 page = "forgot";


                String Emailstr = email.getText().toString();

                queue = Volley.newRequestQueue(forgotpassword.this);

                final String URL = "https://support.stonegroup.co.uk/BarcodeAppPHP/forgotpass.php?email="+Emailstr;



                StringRequest strreq = new StringRequest(Request.Method.GET, URL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // get response
                                Log.v("got", response);
                                //  Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
                                try {
                                    // Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();

                                    JSONArray jsonarray = new JSONArray(response);

                                    HashMap<String, String> item;
                                    for (int i = 0; i < jsonarray.length(); i++) {

                                        JSONObject jsonobject = jsonarray.getJSONObject(i);
                                        String res = jsonobject.getString("stat");
                                        Toast.makeText(forgotpassword.this, res, Toast.LENGTH_LONG).show();




                                    }


//


                                    Log.v("HELPP", response);


                                } catch (JSONException e) {

                                    Log.e("JSON Parser", "Error parsing data " + e.toString());
                                    String m = "Sorry either your E-mail or Password is wrong please try again.";
                                    Toast.makeText(forgotpassword.this, m, Toast.LENGTH_LONG).show();



                                }


                            }


                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError e) {
                        e.printStackTrace();
                    }
                });


                queue.add(strreq);
                thankyoupage();








            }



        });






            }


            public void thankyoupage(){

                Intent i = new Intent(forgotpassword.this, thankyourepair.class);
                Bundle bun = new Bundle();
                bun.putString("type", page);
                i.putExtras(bun);
                startActivity(i);



            }




}
